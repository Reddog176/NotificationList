import android.content.Context;
import android.content.res.Configuration;
import android.support.test.InstrumentationRegistry;

import com.holgis.portalnotifier.R;
import com.holgis.portalnotifier.data.Dataset;
import com.holgis.portalnotifier.data.Entry;

import org.junit.Test;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;


/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class AttackMessageParserTest {

    @Test
    public void basicAttackGerman() {

        String message = "Dein Portal Grafitti on an Old Railroad Wagon (Musenbergstra&#223;e 16A, 81929 M&#252;nchen, Germany) wird von Panzerkiller100 angegriffen.";

        String matchAttack = "(Dein Portal )(.*)[(](.*)[)]( wird von )(.*)( angegriffen.)";
        Pattern p = Pattern.compile(matchAttack);
        Matcher m = p.matcher(message);
        if(m.find()){
            int count = m.groupCount();
            assertEquals(6, count);
            String portal = m.group(2).trim();
            String address = m.group(3).trim();
            String agent = m.group(5).trim();

            assertEquals("Grafitti on an Old Railroad Wagon", portal);
            assertEquals("Musenbergstra&#223;e 16A, 81929 M&#252;nchen, Germany", address);
            assertEquals("Panzerkiller100", agent);
        }
    }

    @Test
    public void basicAttackEnglish() {

        String message = "Your portal Grafitti on an Old Railroad Wagon (Musenbergstra&#223;e 16A, 81929 M&#252;nchen, Germany) is under attack by ToniTankwart.";

        String matchAttack = "(Your portal )(.*)[(](.*)[)]( is under attack by )(.*)(.)";
        Pattern p = Pattern.compile(matchAttack);
        Matcher m = p.matcher(message);
        if(m.find()){
            int count = m.groupCount();
            assertEquals(6, count);
            String portal = m.group(2).trim();
            String address = m.group(3).trim();
            String agent = m.group(5).trim();

            assertEquals("Grafitti on an Old Railroad Wagon", portal);
            assertEquals("Musenbergstra&#223;e 16A, 81929 M&#252;nchen, Germany", address);
            assertEquals("ToniTankwart", agent);
        }
    }

    @Test
    public void parsePortal(){
        String title = "Grafitti on an Old Railroad Wagon (Musenbergstra&#223;e 16A, 81929 M&#252;nchen, Germany)";
        String text = "Your portal Grafitti on an Old Railroad Wagon (Musenbergstra&#223;e 16A, 81929 M&#252;nchen, Germany) is under attack by ToniTankwart.";

        Dataset dataset = new Dataset();
        Entry entry = dataset.ParseEntryPrime(InstrumentationRegistry.getContext(), title, text, (long) 0);

        dataset.SaveEntry(entry);
    }

}
