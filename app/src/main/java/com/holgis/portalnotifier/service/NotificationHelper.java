package com.holgis.portalnotifier.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;

import com.holgis.portalnotifier.R;
import com.holgis.portalnotifier.ui.NotificationActivity;

import java.util.Arrays;

public class NotificationHelper {

	static public final int TYPE_MESSAGE = 0;
	static public final int TYPE_ATTACK = 1;

	private Notification.Builder mBuilder;

	private SharedPreferences mPrefs;

	private Context context;
	private Uri mSound;
	private boolean mVibrate = false;
	private int mLightColor = 0;
	private int mImportance = NotificationManager.IMPORTANCE_DEFAULT;

	//@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public NotificationHelper(Context context) {
		this.context = context;
		this.mPrefs = PreferenceManager.getDefaultSharedPreferences(context);

		this.mBuilder = new Notification.Builder(context);

		setIntent(context);
		
		if (Build.VERSION.SDK_INT >= 21) {
			mBuilder.setVisibility(Notification.VISIBILITY_PUBLIC);
		}

        mBuilder.setAutoCancel(true);
	}

	public static String CHANNEL_EVENT = "channelEvent";
	public static String CHANNEL_MESSAGE = "channelMessage";
	public static String CHANNEL_FAVORITE = "channelFavorite";

    public static void buildChannel(Context context, String channelId) {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

			NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			if(notificationManager != null) {
				NotificationChannel channel = notificationManager.getNotificationChannel(channelId);
				if (channel == null) {

					CharSequence name = context.getResources().getString(R.string.channel_name_event);
					String description = context.getResources().getString(R.string.channel_description_event);
					if (CHANNEL_FAVORITE.equalsIgnoreCase(channelId)) {
						name = context.getResources().getString(R.string.channel_name_favorite);
						description = context.getResources().getString(R.string.channel_description_favorite);
					} else if (CHANNEL_MESSAGE.equalsIgnoreCase(channelId)) {
						name = context.getResources().getString(R.string.channel_name_message);
						description = context.getResources().getString(R.string.channel_description_message);
					}

					channel = new NotificationChannel(channelId, name, NotificationManager.IMPORTANCE_DEFAULT);
					channel.setDescription(description);

					if (CHANNEL_FAVORITE.equalsIgnoreCase(channelId)) {
						final long pattern[] = {100, 250, 100, 250, 100, 250};
						channel.enableVibration(true);
						channel.setVibrationPattern(pattern);
					}

					channel.enableLights(true);
					if (CHANNEL_MESSAGE.equalsIgnoreCase(channelId)) {
						channel.setLightColor(Color.BLUE);
					} else {
						channel.setLightColor(Color.RED);
					}

					AudioAttributes attributes = new AudioAttributes.Builder()
							.setContentType(AudioAttributes.CONTENT_TYPE_UNKNOWN)
							.setUsage(AudioAttributes.USAGE_NOTIFICATION)
							.build();

					channel.setSound(RingtoneManager
							.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), attributes); // This is IMPORTANT

					// Register the channel with the system; you can't change the importance
					// or other notification behaviors after this
					notificationManager.createNotificationChannel(channel);
				}
			}
		}

    }

	public Notification Build(String channelId) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			mBuilder.setChannelId(channelId);
		}
		return mBuilder.build();
	}

	public void SetType(int type) {

		if (type == TYPE_MESSAGE) {
			mBuilder.setSmallIcon(R.drawable.ic_stat_chat);
		} else {
			mBuilder.setSmallIcon(R.drawable.ic_stat_attack);
		}
	}

	public void SetPriority(int priority) {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			if (priority > 0) {
				mImportance = NotificationManager.IMPORTANCE_HIGH;
			} else if (priority < 0) {
				mImportance = NotificationManager.IMPORTANCE_LOW;
			} else {
				mImportance = NotificationManager.IMPORTANCE_DEFAULT;
			}
		} else {
			mBuilder.setPriority(priority);
		}
    }

	public void SetVibration(boolean enable) {

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
			final long pattern[] = {100, 250, 100, 250, 100, 250};
			final long off[] = {0};

			mBuilder.setVibrate(enable ? pattern : off);
		}
	}


	public void SetLight(int type) {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			if (type == TYPE_MESSAGE) {
				mLightColor = Color.BLUE;
			} else {
				mLightColor = Color.RED;
			}
		} else {
			if (type == TYPE_MESSAGE) {
				mBuilder.setLights(Color.BLUE, 300, 200);
			} else {
				mBuilder.setLights(Color.RED, 500, 1500);
			}
		}

	}

	public void SetTone(Uri uri) {
		if (uri != null) {
    		mSound = uri;
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
				mBuilder.setSound(uri);
			}
		}
	}

	@TargetApi(Build.VERSION_CODES.KITKAT_WATCH)
	public void SetOnWear(boolean onWear) {
		if (Build.VERSION.SDK_INT >= 20) {
			mBuilder.setLocalOnly(!onWear);
		}
	}

	public void SetMessage(String title, String message) {
		mBuilder.setContentTitle(title);
		mBuilder.setContentText(message);
	}

	private void setIntent(Context context) {

		Intent i = null;

		if (mPrefs.getBoolean("LAUNCH_INGRESS", false)) {

			i = context.getPackageManager().getLaunchIntentForPackage(
					"com.nianticlabs.ingress.prime.qa");
			if(i == null){
				i = context.getPackageManager().getLaunchIntentForPackage(
						"com.nianticproject.ingress");
			}

		} else {
			i = new Intent(context, NotificationActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP
					| Intent.FLAG_ACTIVITY_CLEAR_TASK
					| Intent.FLAG_ACTIVITY_NEW_TASK);
		}

		if (i != null) {

			PendingIntent pi = PendingIntent.getActivity(context, 0, i,
					PendingIntent.FLAG_UPDATE_CURRENT);

			mBuilder.setContentIntent(pi);
		}
	}

}
