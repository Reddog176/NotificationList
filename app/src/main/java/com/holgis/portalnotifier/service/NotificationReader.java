package com.holgis.portalnotifier.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.text.Html;
import android.widget.RemoteViews;

public class NotificationReader {

	static public Bundle parse19(Context context, StatusBarNotification notification) {

		Bundle extra = notification.getNotification().extras;

		String title = extra.getString(Notification.EXTRA_TITLE);
		String text = extra.getString(Notification.EXTRA_TEXT);
		CharSequence[] lines = extra
				.getCharSequenceArray(Notification.EXTRA_TEXT_LINES);

		boolean prime = false;
		String json = "";
		Object oJson = extra.get("json");
        if(oJson!=null){
            json = oJson.toString();
            saveJson(context, json);
        }

		if(title == null) {
			Object spannableTitle = extra.get(Notification.EXTRA_TITLE);
			if(spannableTitle != null) {
				title = fromHtml(spannableTitle);
                prime = true;
			}
		}

		if(text == null) {
			Object spannableText = extra.get(Notification.EXTRA_TEXT);
			if(spannableText != null) {
				text = fromHtml(spannableText);
                prime = true;
			}
		}

		Long when = notification.getNotification().when;
        if(when == 0){
			Date now = new Date();
        	when = now.getTime();
		}

		Bundle bundle = new Bundle();
		bundle.putString(NotificationHandler.SBN_EXTRA_TITLE, title);
		bundle.putString(NotificationHandler.SBN_EXTRA_TEXT, text);
		bundle.putCharSequenceArray(NotificationHandler.SBN_EXTRA_LINES, lines);
		bundle.putLong(NotificationHandler.SBN_EXTRA_DATE, when);
        bundle.putBoolean(NotificationHandler.SBN_EXTRA_PRIME, prime);
        bundle.putString(NotificationHandler.SBN_EXTRA_JSON, json);

		return bundle;
	}


	static private String fromHtml(Object spannable){
		if (Build.VERSION.SDK_INT >= 24) {
			return Html.fromHtml(spannable.toString(), Html.FROM_HTML_MODE_LEGACY).toString();
		} else {
			return Html.fromHtml(spannable.toString()).toString();
		}
	}
    static private void saveJson(Context context, String json) {

		/*  //requires external storage permission
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		if (prefs.getBoolean("DEBUG_NOTIFICATION", false)) {

			String date = DateFormat.format("dd-MM-yyyy-hh-mm-ss", new Date()).toString();

			File playlistFolder = context.getExternalFilesDir("json");
			File file = new File(playlistFolder, date + ".json");

			try (Writer writer = new FileWriter(file)) {
				writer.write(json);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		*/
    }

    static public Bundle parse00(Context context, StatusBarNotification notification) {

		Long when = notification.getNotification().when;

		Bundle bundle = new Bundle();
		bundle.putString(NotificationHandler.SBN_EXTRA_TITLE, "<title>");
		bundle.putString(NotificationHandler.SBN_EXTRA_TEXT, "<text>");
		bundle.putStringArray(NotificationHandler.SBN_EXTRA_LINES,
				new String[] { "<line1>", "<line2>", "<line3>" });
		bundle.putLong(NotificationHandler.SBN_EXTRA_DATE, when);

		return bundle;
	}

	@SuppressLint("UseSparseArrays")
	@SuppressWarnings({ "unchecked" })
	private void extract(Notification notification) {

		RemoteViews views = notification.contentView;
		@SuppressWarnings("rawtypes")
		Class secretClass = views.getClass();

		try {
			Map<Integer, String> text = new HashMap<Integer, String>();

			Field outerFields[] = secretClass.getDeclaredFields();

			for (int i = 0; i < outerFields.length; i++) {

				if (!outerFields[i].getName().equals("mActions"))
					continue;

				outerFields[i].setAccessible(true);

				ArrayList<Object> actions = (ArrayList<Object>) outerFields[i]
						.get(views);

				if (actions != null) {
					for (Object action : actions) {
						if (action != null) {

							Field innerFields[] = action.getClass()
									.getDeclaredFields();

							Object value = null;
							Integer type = null;
							Integer viewId = null;
							if (innerFields != null) {
								for (Field field : innerFields) {
									if (field != null) {
										field.setAccessible(true);
										if (field.getName().equals("value")) {
											value = field.get(action);
										} else if (field.getName().equals(
												"type")) {
											type = field.getInt(action);
										} else if (field.getName().equals(
												"viewId")) {
											viewId = field.getInt(action);
										}
									}
								}
							}

							if (type != null && (type == 9 || type == 10)) {
								text.put(viewId, value.toString());
							}
						}
					}
				}

				System.out.println("title is: " + text.get(16908310));
				System.out.println("info is: " + text.get(16909082));
				System.out.println("text is: " + text.get(16908358));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
