package com.holgis.portalnotifier.service;

import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;

import com.holgis.portalnotifier.App;
import com.holgis.portalnotifier.data.Dataset;
import com.holgis.portalnotifier.data.Entry;
import com.holgis.portalnotifier.data.Player;
import com.holgis.portalnotifier.data.Portal;
import com.holgis.portalnotifier.ui.frag.DisturbFragment;

import org.greenrobot.eventbus.EventBus;


public class NotificationHandler extends BroadcastReceiver {

	public static String SBN_ACTION = "com.holgis.notificationlist.NOTIFICATION_RECIVED";
	public static String SBN_UPDATE = "com.holgis.notificationlist.NOTIFICATION_UPDATE";

	public static String SBN_EXTRA_TITLE = "com.holgis.notificationlist.SBN.Title";
	public static String SBN_EXTRA_TEXT = "com.holgis.notificationlist.SBN.Text";
	public static String SBN_EXTRA_LINES = "com.holgis.notificationlist.SBN.Lines";
    public static String SBN_EXTRA_DATE = "com.holgis.notificationlist.SBN.Date";
    public static String SBN_EXTRA_PRIME = "com.holgis.notificationlist.SBN.Prime";
    public static String SBN_EXTRA_JSON = "com.holgis.notificationlist.SBN.Json";

    private static String SBN_PACKAGE = "com.nianticproject.ingress";
	private static String SBN_PACKAGE2 = "com.nianticlabs.ingress.prime.qa";

	private final int NOTIFY_ID_CHAT = 145212;
	private final int NOTIFY_ID_ATTACK = 145213;
	private final int NOTIFY_ID_FAVORITE = 145214;

	private Dataset dataset = new Dataset();

	static public boolean isHandling(String packageName) {
		return SBN_PACKAGE.equalsIgnoreCase(packageName) ||
				SBN_PACKAGE2.equalsIgnoreCase(packageName);
	}

	@Override
	public void onReceive(Context context, Intent intent) {

		Bundle extras = intent.getExtras();

		if (extras != null) {
			Entry entry = parseExtras(context, extras);

			if (entry != null) {

				notify(context, entry);

				EventBus.getDefault().post(new UpdateEvent());
			}
		}
	}

	private Entry parseExtras(Context context, Bundle extras) {

        boolean prime = extras.getBoolean(NotificationHandler.SBN_EXTRA_PRIME);

        Entry e = null;

		String title = extras.getString(NotificationHandler.SBN_EXTRA_TITLE);
		String text = extras.getString(NotificationHandler.SBN_EXTRA_TEXT);

		CharSequence[] lines = extras
				.getCharSequenceArray(NotificationHandler.SBN_EXTRA_LINES);

		if(!prime) {

            if (text != null) {
                // COMM message
            } else {
                if (lines != null) {
                    String line = lines[0].toString();
                    if (title != null && title.contains("new COMM messages")) {
                        int space = line.indexOf(" ");
                        title = line.substring(0, space).trim();
                        text = line.substring(space).trim();
                    } else
                        text = line;
                }
            }
        }

		Long when = extras.getLong(NotificationHandler.SBN_EXTRA_DATE);

		try {

		    if (prime) {
		        e = dataset.ParseEntryPrime(context, title, text, (long) when);
            } else {
                e = dataset.ParseEntry(context, title, text, (long) when);
            }
			dataset.SaveEntry(e);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return e;
	}


    public static void TestNotification(Context context, int type, boolean favorite) {

		Entry entry = new Entry();

		entry.setType(type);

		long t = new Date().getTime();
		entry.setDate(t);
		entry.setTime(DisturbFragment.formatTime(context, t));

		Player player = new Player();
		player.setName("Agent_Name");
		player.setFavourite(favorite);

		entry.setPlayer(player);

		if (type == Dataset.TYPE_CHAT) {

			entry.setMessage("This is a test!");

		} else {

			Portal portal = new Portal();

			portal.setName("Portal_Test");
			portal.setAddress("Test Street 123, Test City 12345");
			portal.setFavourite(favorite);

			if (type == Dataset.TYPE_DESTROY) {
				portal.setFavourite(true);
			}

			entry.setPortal(portal);
		}


		NotificationHandler nh = new NotificationHandler();
		nh.notify(context, entry);

	}

	private void notify(Context context, Entry entry) {

		try {

			int id = NOTIFY_ID_ATTACK;

			boolean sendNotification = false;

			boolean doVibrate = false;
			boolean doLight = false;
			boolean doWear = false;

			int priority = Notification.PRIORITY_LOW;

			Uri toneUri = null;

			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(context);

			NotificationHelper helper = new NotificationHelper(context);

			if (entry.getType() == Dataset.TYPE_CHAT) {

				sendNotification = prefs.getBoolean("MESSAGE_ENABLE", true);
				toneUri = getTone(prefs, "MESSAGE_TONE");
				doVibrate = prefs.getBoolean("MESSAGE_VIB", true);
				doLight = prefs.getBoolean("MESSAGE_LED", true);
				doWear = prefs.getBoolean("MESSAGE_WEAR", true);
				priority = Integer.parseInt(prefs.getString("MESSAGE_PRIO",
						Integer.toString(Notification.PRIORITY_DEFAULT)));

				id = NOTIFY_ID_CHAT;

				helper.SetType(NotificationHelper.TYPE_MESSAGE);
				helper.SetMessage(entry.getPlayer().getName(),
						entry.getMessage());

				if (!sendNotification) {

					if (entry.getPlayer().isFavourite()) {
						sendNotification = prefs.getBoolean("FAV_ENABLE", true);
					}
				}

				if (entry.getPlayer().isIgnore()) {
					sendNotification = false;
				}

			} else {

				sendNotification = prefs.getBoolean("EVENT_ENABLE", true);
				toneUri = getTone(prefs, "EVENT_TONE");
				doVibrate = prefs.getBoolean("EVENT_VIB", false);
				doLight = prefs.getBoolean("EVENT_LED", false);
				doWear = prefs.getBoolean("EVENT_WEAR", false);
				priority = Integer.parseInt(prefs.getString("EVENT_PRIO",
						Integer.toString(Notification.PRIORITY_LOW)));

				helper.SetType(NotificationHelper.TYPE_ATTACK);

				String message = "";
				if (entry.getType() == Dataset.TYPE_ATTACK) {
					message = "attacked by " + entry.getPlayer().getName();
				} else {
					message = "neutralized by " + entry.getPlayer().getName();
				}
				helper.SetMessage(entry.getPortal().getName(), message);

				boolean isFavourite = entry.getPortal().isFavourite()
						|| entry.getPlayer().isFavourite();

				if (isFavourite) {

					id = NOTIFY_ID_FAVORITE;

					toneUri = getTone(prefs, "FAV_TONE");
					doVibrate = prefs.getBoolean("FAV_VIB", true);
					doLight = prefs.getBoolean("FAV_LED", true);
					doWear = prefs.getBoolean("FAV_WEAR", true);
					priority = Integer.parseInt(prefs.getString("FAV_PRIO",
							Integer.toString(Notification.PRIORITY_HIGH)));

					sendNotification = prefs.getBoolean("FAV_ENABLE", true);
				}

				if (entry.getPortal().isIgnore()
						|| (entry.getPlayer().isIgnore() && !entry.getPortal()
								.isFavourite())) {
					sendNotification = false;
				}

			}

			if (sendNotification) {

				if (isDoNotDisturb(context)) {
					helper.SetTone(null);
					helper.SetVibration(false);
					helper.SetOnWear(false);
					helper.SetPriority(Notification.PRIORITY_DEFAULT);
				} else {
					helper.SetTone(toneUri);
					helper.SetVibration(doVibrate);
					helper.SetOnWear(doWear);
					helper.SetPriority(priority);
				}

				if (doLight) {
					helper.SetLight(entry.getType());
				}

				NotificationManager mNotificationManager = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);

				// mId allows you to update the notification later on.

				String channelId = getNotificationChannelId(id);
				mNotificationManager.notify(id, helper.Build(channelId));

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private String getNotificationChannelId(int notificationTypeId){
		switch(notificationTypeId) {
				default:
				case NOTIFY_ID_ATTACK:
					return NotificationHelper.CHANNEL_EVENT;
				case NOTIFY_ID_CHAT:
					return NotificationHelper.CHANNEL_MESSAGE;
				case NOTIFY_ID_FAVORITE:
					return NotificationHelper.CHANNEL_FAVORITE;
		}
	}

	private Uri getTone(SharedPreferences prefs, String tone) {

		Uri uri = null;
		String str = prefs.getString(tone, "*");
		if ("*".equals(str)) {
			uri = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		} else {
			if (!str.isEmpty()) {
				try {
					uri = Uri.parse(str);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		return uri;
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	private static boolean isDoNotDisturb(Context context) {

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(context);

		if (prefs.getBoolean("DISTURB_ENABLE", false)) {
			boolean isOn = false;

			try {
				PowerManager pm = (PowerManager) context
						.getSystemService(Context.POWER_SERVICE);

				if (Build.VERSION.SDK_INT >= 20) {
					isOn = pm.isInteractive();
				} else {
					isOn = pm.isScreenOn();
				}
			} catch (Exception e) {
			}

			if (!isOn) {

				long startTime = prefs.getLong("DISTURB_START", 0);
				long stopTime = prefs.getLong("DISTURB_END", 0);

				Calendar cal = Calendar.getInstance();
				int hour = cal.get(Calendar.HOUR_OF_DAY);
				int min = cal.get(Calendar.MINUTE);

				long now = hour * 100 + min;

				if (now >= startTime && now <= stopTime) {
					return true;
				}
			}
		}

		return false;
	}
}
