package com.holgis.portalnotifier.service;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.content.LocalBroadcastManager;


public class NotificationService extends NotificationListenerService {

	private NotificationHandler mHandler;

	@Override
	public void onCreate() {
		super.onCreate();

		mHandler = new NotificationHandler();

		IntentFilter filter = new IntentFilter();
		filter.addAction(NotificationHandler.SBN_ACTION);

		LocalBroadcastManager.getInstance(this).registerReceiver(mHandler,
				filter);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		LocalBroadcastManager.getInstance(this).unregisterReceiver(mHandler);
	}

	@Override
	public IBinder onBind(Intent intent) {

		return super.onBind(intent);
	}

	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	
	@Override
	public void onNotificationPosted(StatusBarNotification notification) {

		if (NotificationHandler.isHandling(notification.getPackageName())) {

			try {

				Intent i = new Intent(NotificationHandler.SBN_ACTION);
				if (Build.VERSION.SDK_INT >= 19) {
					i.putExtras(NotificationReader.parse19(this, notification));
				} else {
					i.putExtras(NotificationReader.parse00(this, notification));
				}
				LocalBroadcastManager.getInstance(this).sendBroadcast(i);

			} catch (Exception e) {
			}

			if (PreferenceManager.getDefaultSharedPreferences(
					getApplicationContext()).getBoolean("CLEAR_NOTIFICATION",
					true)) {

				if (Build.VERSION.SDK_INT >= 21) {
					cancelNotification(notification.getKey());
				} else {
					cancelNotification(notification.getPackageName(),
							notification.getTag(), notification.getId());
				}
			}
		}
	}

	@Override
	public void onNotificationRemoved(StatusBarNotification notification) {

	}

	public static boolean IsRegistered(Context context) {

		final String flat = Settings.Secure.getString(
				context.getContentResolver(), "enabled_notification_listeners");
		boolean enabled = false;
		if (flat != null && !"".equals(flat)) {
			final String[] names = flat.split(":");
			for (int i = 0; i < names.length; i++) {
				if (names[i].contains(context.getPackageName())) {
					enabled = true;
				}
			}
		}

		return enabled;
	}

}
