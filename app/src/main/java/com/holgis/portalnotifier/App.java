package com.holgis.portalnotifier;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.text.Html;

import com.holgis.portalnotifier.data.DBHelper;
import com.holgis.portalnotifier.data.Dataset;
import com.holgis.portalnotifier.data.Entry;
import com.holgis.portalnotifier.service.NotificationHelper;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;

public class App extends Application {

	private DBHelper dbHelper;
	private SQLiteDatabase db;

	private static App mInstance;

	@Override
	public void onCreate() {
		super.onCreate();

		NotificationHelper.buildChannel(this, NotificationHelper.CHANNEL_EVENT);
		NotificationHelper.buildChannel(this, NotificationHelper.CHANNEL_FAVORITE);
		NotificationHelper.buildChannel(this, NotificationHelper.CHANNEL_MESSAGE);

		// Actually, this should not be done in the main thread
		loadDB();

		mInstance = this;
	}

	@Override
	public void onTerminate() {
		
		dbHelper.close();
		
		super.onTerminate();
	}

	public static SQLiteDatabase getDb() {
		
		if(mInstance.db==null){
			mInstance.loadDB();
		}
		return mInstance.db;
	}

	public static void registerNotificationService(Context context) {
		try {
			Intent i = new Intent(
					"android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void applySelectedTheme(Activity activity) {

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(activity);

		if (prefs.getBoolean("DARK", true)) {
			activity.setTheme(R.style.AppThemeDark);
		} else {
			activity.setTheme(R.style.AppTheme);
		}
	}

	public static void applySelectedTheme(PreferenceActivity activity) {

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(activity.getApplicationContext());

		if (prefs.getBoolean("DARK", true)) {
			activity.setTheme(R.style.AppThemeDark);
		} else {
			activity.setTheme(R.style.AppTheme);
		}
	}

	private void loadDB() {

		if (dbHelper == null) {
			try {
				dbHelper = new DBHelper(getApplicationContext());
			} catch (Exception e) {
				dbHelper = null;
			}
		}
		
		if (dbHelper != null) {
			if (db == null) {
				try {
					db = dbHelper.getWritableDatabase();
				} catch (Exception e) {
					db = null;
				}
			}
		}
		
	}

}
