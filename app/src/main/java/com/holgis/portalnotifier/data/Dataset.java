package com.holgis.portalnotifier.data;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import com.holgis.portalnotifier.App;
import com.holgis.portalnotifier.R;

public class Dataset {

	public static final int TYPE_ATTACK = 1;
	public static final int TYPE_DESTROY = 2;
	public static final int TYPE_CHAT = 3;

	public SQLiteCursor QueryEventsAll() {

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM event "
				+ "LEFT JOIN player ON event.player = player._id "
				+ "LEFT JOIN portal ON event.portal = portal._id "
				+ "ORDER BY event.date DESC";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {});
		return cursor;
	}

	public SQLiteCursor QueryEventsPortal() {

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM event "
				+ "LEFT JOIN player ON event.player = player._id "
				+ "LEFT JOIN portal ON event.portal = portal._id "
				+ "WHERE event.type != 3 ORDER BY event.date DESC";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {});
		return cursor;
	}

	public SQLiteCursor QueryEventsPortalFav() {

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM event "
				+ "LEFT JOIN player ON event.player = player._id "
				+ "LEFT JOIN portal ON event.portal = portal._id "
				+ "WHERE event.type != 3 AND portal.fav = 1 "
				+ "ORDER BY event.date DESC";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {});
		return cursor;
	}

	public SQLiteCursor QueryEventsMessages() {

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM event "
				+ "LEFT JOIN player ON event.player = player._id "
				+ "LEFT JOIN portal ON event.portal = portal._id "
				+ "WHERE event.type = 3 ORDER BY event.date DESC";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {});
		return cursor;
	}

	public SQLiteCursor QueryEventsMessagesFav() {

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM event "
				+ "LEFT JOIN player ON event.player = player._id "
				+ "LEFT JOIN portal ON event.portal = portal._id "
				+ "WHERE event.type = 3 AND player.fav = 1 "
				+ "ORDER BY event.date DESC";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {});
		return cursor;
	}

	public SQLiteCursor QueryEventsFav() {

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM event "
				+ "LEFT JOIN player ON event.player = player._id "
				+ "LEFT JOIN portal ON event.portal = portal._id "
				+ "WHERE portal.fav = 1 OR player.fav = 1 "
				+ "ORDER BY event.date DESC";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {});
		return cursor;
	}

	public SQLiteCursor QueryPortalsFav() {

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM portal WHERE fav = 1 ORDER BY name ASC";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {});
		return cursor;
	}

	public SQLiteCursor QueryPortals() {

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM portal ORDER BY name ASC";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {});
		return cursor;
	}

	public SQLiteCursor QueryPlayersFav() {

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM player WHERE fav = 1 ORDER BY name ASC";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {});
		return cursor;
	}

	public SQLiteCursor QueryPlayers() {

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM player ORDER BY name ASC";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {});
		return cursor;
	}

	public SQLiteCursor QueryCustoms() {

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM custom ORDER BY name ASC";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {});
		return cursor;
	}

	public boolean MatchCustomFavs(String name, String address) {

		boolean match = false;

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT _id FROM custom WHERE fav = 1 AND ? LIKE match_name AND ? LIKE match_address";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {
				name.toLowerCase(Locale.getDefault()).trim(),
				address.toLowerCase(Locale.getDefault()).trim() });

		if (cursor != null) {

			if (cursor.moveToFirst()) {
				match = true;
			}
			cursor.close();
		}
		return match;
	}

	public boolean MatchCustomIgnores(String name, String address) {

		boolean match = false;

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT _id FROM custom WHERE fav = -1 AND ? LIKE match_name AND ? LIKE match_address";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {
				name.toLowerCase(Locale.getDefault()).trim(),
				address.toLowerCase(Locale.getDefault()).trim() });

		if (cursor != null) {

			if (cursor.moveToFirst()) {
				match = true;
			}
			cursor.close();
		}
		return match;
	}

	public void DeleteEntrys(long daysOld) {
		if (daysOld > 0) {

			long then = new Date().getTime() - (24 * 60 * 60 * 1000 * daysOld);

			SQLiteDatabase db = App.getDb();
			db.delete("event", "date < ?", new String[] { Long.toString(then) });
		}
	}

	public Entry LoadEntry(Cursor cursor) {

		return loadEntry(cursor, 0);
	}

	public long SaveEntry(Entry entry) {

		long entry_id = -1;

		if (entry != null) {

			SQLiteDatabase db = App.getDb();

			try {
				ContentValues values = new ContentValues();

				values.put("time", entry.getTime());
				values.put("type", entry.getType());
				values.put("date", entry.getDate());

				values.put("player", savePlayer(db, entry.getPlayer()));

				if (entry.getType() == TYPE_CHAT) {
					values.put("message", entry.getMessage());
					values.put("portal", 0);
				} else {
					values.put("message", "");
					values.put("portal", savePortal(db, entry.getPortal()));
				}

				entry_id = db.insert("event", null, values);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return entry_id;
	}

	public void UpdateEntry(Entry entry) {

		if (entry != null) {

			SQLiteDatabase db = App.getDb();

			if (entry.getType() == TYPE_CHAT) {
				updatePlayer(db, entry.getPlayer());
			} else {
				updatePortal(db, entry.getPortal());
			}
		}

	}

	public long savePlayer(SQLiteDatabase db, Player player) {

		long player_id = player.getId();

		if (player_id == -1) {

			try {

				ContentValues values = new ContentValues();

				values.put("name", player.getName());
				values.put("fav", player.isFavourite() ? 1
						: player.isIgnore() ? -1 : 0);

				player_id = db.insert("player", null, values);

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			updatePlayer(db, player);
		}

		return player_id;
	}

	public void updatePlayer(SQLiteDatabase db, Player player) {

		try {

			ContentValues values = new ContentValues();

			values.put("name", player.getName());
			values.put("fav", player.isFavourite() ? 1 : player.isIgnore() ? -1
					: 0);

			db.update("player", values, "_id = ?",
					new String[] { Integer.toString(player.getId()) });

		} catch (Exception e) {
			e.printStackTrace();
		}

		return;
	}

	public long savePortal(SQLiteDatabase db, Portal portal) {

		long portal_id = portal.getId();

		if (portal_id == -1) {

			try {

				ContentValues values = new ContentValues();

				values.put("name", portal.getName());
				values.put("address", portal.getAddress());
				values.put("fav", portal.isFavourite() ? 1
						: portal.isIgnore() ? -1 : 0);

				double lng = 0.0;
				double lat = 0.0;
				int gps = 0;
				if (portal.hasLocation()) {
					lng = portal.getLocation().getLongitude();
					lat = portal.getLocation().getLatitude();
					gps = 1;
				}
				values.put("gps", gps);
				values.put("longitude", lng);
				values.put("latitude", lat);

				portal_id = db.insert("portal", null, values);

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {

			updatePortal(db, portal);

		}

		return portal_id;
	}

	public void updatePortal(SQLiteDatabase db, Portal portal) {

		try {

			ContentValues values = new ContentValues();

			values.put("name", portal.getName());
			values.put("address", portal.getAddress());
			values.put("fav", portal.isFavourite() ? 1 : portal.isIgnore() ? -1
					: 0);

			double lng = 0.0;
			double lat = 0.0;
			int gps = 0;
			if (portal.hasLocation()) {
				lng = portal.getLocation().getLongitude();
				lat = portal.getLocation().getLatitude();
				gps = 1;
			}
			values.put("gps", gps);
			values.put("longitude", lng);
			values.put("latitude", lat);

			db.update("portal", values, "_id = ?",
					new String[] { Integer.toString(portal.getId()) });

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Entry ParseEntry(Context context, String title, String text,
			long date) {

		String time = getTime(context, date);

		SQLiteDatabase db = App.getDb();

		Entry entry = new Entry();
		entry.setTime(parseTime(context, time));
		entry.setDate(date);

		int type = parseType(title);
		entry.setType(type);

		String strPlayer = parsePlayer(type, title, text);
		Player player = loadPlayer(db, strPlayer);
		entry.setPlayer(player);

		if (type != TYPE_CHAT) {

			Portal portal = loadPortal(db, parsePortal(text),
					parseAddress(text));

			try {
				if (MatchCustomIgnores(portal.getName(), portal.getAddress())) {
					if (!portal.isFavourite()) {
						portal.setIgnore(true);
					}
				}
				if (MatchCustomFavs(portal.getName(), portal.getAddress())) {
					if (!portal.isIgnore()) {
						portal.setFavourite(true);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			entry.setPortal(portal);

		} else {

			entry.setMessage(text);
		}

		return entry;
	}

	private List<String> parseAttackPrime(Context context, String message){

		try {
			String matchAttack = context.getResources().getString(R.string.matcher_pattern_attack_basic);
			Pattern pattern = Pattern.compile(matchAttack);
			Matcher matcher = pattern.matcher(message);
			if (matcher.find()) {

				int groupPortal = context.getResources().getInteger(R.integer.matcher_result_portal);
				int groupAddress = context.getResources().getInteger(R.integer.matcher_result_address);
				int groupAgent = context.getResources().getInteger(R.integer.matcher_result_agent);

				List<String> result = new ArrayList<>();
				result.add(matcher.group(groupPortal).trim());
				result.add(matcher.group(groupAddress).trim());
				result.add(matcher.group(groupAgent).trim());

				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public Entry ParseEntryPrime(Context context, String title, String text, long date) {

		String time = getTime(context, date);

		SQLiteDatabase db = App.getDb();

		Entry entry = new Entry();
		entry.setTime(parseTime(context, time));
		entry.setDate(date);

		int type = text.startsWith("@") ? 3 : 1;
		entry.setType(type);

		if (type != TYPE_CHAT) {

			List<String> result = parseAttackPrime(context, text);
			if(result != null) {

				Player player = loadPlayer(db, result.get(2));
				entry.setPlayer(player);

				Portal portal = loadPortal(db, result.get(0), result.get(1));

				try {
					if (MatchCustomIgnores(portal.getName(), portal.getAddress())) {
						if (!portal.isFavourite()) {
							portal.setIgnore(true);
						}
					}
					if (MatchCustomFavs(portal.getName(), portal.getAddress())) {
						if (!portal.isIgnore()) {
							portal.setFavourite(true);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				entry.setPortal(portal);
			}

		} else {

			Player player = loadPlayer(db, title);
			entry.setPlayer(player);

			int space = text.indexOf(" ");
			entry.setMessage(text.substring(space).trim());
		}

		return entry;
	}

	private Entry loadEntry(Cursor cursor, int columnOffset) {

		int loaded_id = cursor.getInt(columnOffset + 0);
		String loaded_time = cursor.getString(columnOffset + 1);
		int loaded_type = cursor.getInt(columnOffset + 2);
		@SuppressWarnings("unused")
		int loaded_player = cursor.getInt(columnOffset + 3);
		String loaded_message = cursor.getString(columnOffset + 4);
		@SuppressWarnings("unused")
		int loaded_portal = cursor.getInt(columnOffset + 5);
		long loaded_date = cursor.getLong(columnOffset + 6);

		Entry entry = new Entry(loaded_id);

		entry.setTime(loaded_time);
		entry.setType(loaded_type);
		entry.setPlayer(loadPlayer(cursor, 7));
		entry.setMessage(loaded_message);
		entry.setPortal(loadPortal(cursor, 10));
		entry.setDate(loaded_date);

		return entry;
	}

	public Player loadPlayer(SQLiteDatabase db, String name) {

		Player player = null;

		String[] columnsWithId = { "_id", "name", "fav" };

		Cursor cursor = (SQLiteCursor) db.query("player", columnsWithId,
				"name = ?", new String[] { name }, null, null, null);

		if (cursor.moveToFirst()) {

			player = loadPlayer(cursor, 0);

		} else {

			player = new Player();
			player.setName(name);

		}
		cursor.close();

		return player;
	}

	public Player loadPlayer(Cursor cursor, int columnOffset) {

		int loaded_id = cursor.getInt(columnOffset + 0);
		String loaded_name = cursor.getString(columnOffset + 1);
		int loaded_fav = cursor.getInt(columnOffset + 2);

		Player player = new Player(loaded_id);
		player.setName(loaded_name);
		player.setFavourite(loaded_fav == 1);
		player.setIgnore(loaded_fav == -1);

		return player;
	}

	public Portal loadPortal(SQLiteDatabase db, String name, String address) {

		Portal portal = null;

		String[] columnsWithId = { "_id", "name", "address", "gps",
				"longitude", "latitude", "fav" };

		Cursor cursor = (SQLiteCursor) db.query("portal", columnsWithId,
				"name = ? AND address = ?", new String[] { name, address },
				null, null, null);

		if (cursor.moveToFirst()) {

			portal = loadPortal(cursor, 0);

		} else {

			portal = new Portal();
			portal.setName(name);
			portal.setAddress(address);

		}
		cursor.close();

		return portal;
	}

	public Portal loadPortal(Cursor cursor, int columnOffset) {

		int loaded_id = cursor.getInt(columnOffset + 0);

		String loaded_name = cursor.getString(columnOffset + 1);
		String loaded_address = cursor.getString(columnOffset + 2);

		int loaded_gps = cursor.getInt(columnOffset + 3);
		double loaded_lng = cursor.getDouble(columnOffset + 4);
		double loaded_lat = cursor.getDouble(columnOffset + 5);

		int loaded_fav = cursor.getInt(columnOffset + 6);

		Portal portal = new Portal(loaded_id);

		portal.setName(loaded_name);
		portal.setAddress(loaded_address);

		if (loaded_gps != 0) {
			Location loc = new Location("");
			loc.setLongitude(loaded_lng);
			loc.setLatitude(loaded_lat);
			portal.setLocation(loc);
		}

		portal.setFavourite(loaded_fav == 1);
		portal.setIgnore(loaded_fav == -1);

		return portal;
	}

	public Custom LoadCustom(Cursor cursor) {

		int loaded_id = cursor.getInt(0);

		String name = cursor.getString(1);
		String address = cursor.getString(2);
		// String match_name = cursor.getString(3);
		// String match_address = cursor.getString(4);
		int loaded_fav = cursor.getInt(5);

		Custom portal = new Custom(loaded_id);

		portal.setName(name);
		portal.setAddress(address);

		portal.setFavourite(loaded_fav == 1);
		portal.setIgnore(loaded_fav == -1);

		return portal;
	}

	public Custom LoadCustom(int id) {

		Custom custom = null;

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM custom WHERE _id == ?";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql,
				new String[] { Integer.toString(id) });

		if (cursor != null) {
			if (cursor.moveToFirst()) {
				custom = LoadCustom(cursor);
			}
			cursor.close();
		}

		return custom;
	}

	public Custom FindCustom(String name, String address) {

		Custom custom = null;

		SQLiteDatabase db = App.getDb();

		String sql = "SELECT * FROM custom WHERE name == ? AND address == ?";

		SQLiteCursor cursor = (SQLiteCursor) db.rawQuery(sql, new String[] {
				name, address });

		if (cursor != null) {
			if (cursor.moveToFirst()) {
				custom = LoadCustom(cursor);
			}
			cursor.close();
		}

		return custom;
	}

	public long SaveCustom(Custom portal) {

		SQLiteDatabase db = App.getDb();

		long portal_id = portal.getId();

		if (portal_id == -1) {

			try {

				ContentValues values = new ContentValues();

				values.put("name", portal.getName());
				values.put("address", portal.getAddress());
				values.put(
						"match_name",
						"%"
								+ portal.getName()
										.toLowerCase(Locale.getDefault())
										.trim() + "%");
				values.put("match_address", "%"
						+ portal.getAddress().toLowerCase(Locale.getDefault())
								.trim() + "%");

				values.put("fav", portal.isFavourite() ? 1
						: portal.isIgnore() ? -1 : 0);

				portal_id = db.insert("custom", null, values);

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			updateCustom(db, portal);
		}

		return portal_id;
	}

	public void DeleteCustom(Custom portal) {
		if (portal != null) {
			if (portal.getId() != -1) {

				SQLiteDatabase db = App.getDb();
				db.delete("custom", "_id == ?",
						new String[] { Integer.toString(portal.getId()) });
			}
		}
	}

	private void updateCustom(SQLiteDatabase db, Custom portal) {

		try {

			ContentValues values = new ContentValues();

			values.put("name", portal.getName());
			values.put("address", portal.getAddress());
			values.put("match_name",
					"%"
							+ portal.getName().toLowerCase(Locale.getDefault())
									.trim() + "%");
			values.put(
					"match_address",
					"%"
							+ portal.getAddress()
									.toLowerCase(Locale.getDefault()).trim()
							+ "%");

			values.put("fav", portal.isFavourite() ? 1 : portal.isIgnore() ? -1
					: 0);

			db.update("custom", values, "_id = ?",
					new String[] { Integer.toString(portal.getId()) });

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int parseType(String title) {
		if (title.contains("under attack")) {
			return 1;
		}
		if (title.contains("neutralized")) {
			return 2;
		}
		return 3;
	}

	private String parseTime(Context context, String time) {
		return now(context);
		/*
		 * String t = time.trim();
		 * 
		 * if (t.isEmpty()) { return now(context); } else { return t; }
		 */
	}

	private String parsePlayer(int type, String title, String text) {
		if (type == 3) {
			return title;
		} else {
			int space = text.indexOf(" ");
			return text.substring(0, space).trim();
		}
	}

	private String parsePortal(String text) {
		int start = text.indexOf(" ");
		int end = text.lastIndexOf("(");
		return text.substring(start, end).trim();
	}

	private String parseAddress(String text) {
		int start = text.lastIndexOf("(");
		int end = text.lastIndexOf(")");
		return text.substring(start + 1, end).trim();
	}

	@SuppressWarnings("unused")
	private void geoCode(Context context, String location) {

		Geocoder coder = new Geocoder(context);
		List<Address> results;

		try {
			results = coder.getFromLocationName(location, 1);
			if (results == null) {
				return;
			}
			Address address = results.get(0);
			address.getLatitude();
			address.getLongitude();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getTime(Context context, Long when) {
		Date date = new Date(when);
		DateFormat dateFormat = android.text.format.DateFormat
				.getDateFormat(context);
		DateFormat timeFormat = android.text.format.DateFormat
				.getTimeFormat(context);
		return dateFormat.format(date) + " " + timeFormat.format(date);
	}

	private String now(Context context) {
		Date date = new Date();
		return getTime(context, date.getTime());
	}

}
