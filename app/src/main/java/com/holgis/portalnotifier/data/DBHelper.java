package com.holgis.portalnotifier.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	public DBHelper(Context context) {
		super(context, "event_db", null, 4);
		
	
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String sql1 = "CREATE TABLE IF NOT EXISTS event(_id INTEGER PRIMARY KEY ASC, time TEXT, type INTEGER, player INTEGER, message TEXT, portal INTEGER, date INTEGER);";
		db.execSQL(sql1);

		String sql2 = "CREATE TABLE IF NOT EXISTS player(_id INTEGER PRIMARY KEY ASC, name TEXT, fav INTEGER);";
		db.execSQL(sql2);

		String sql3 = "CREATE TABLE IF NOT EXISTS portal(_id INTEGER PRIMARY KEY ASC, name TEXT, address TEXT, gps INTEGER, longitude REAL, latitude REAL, fav INTEGER);";
		db.execSQL(sql3);

		String sql4 = "CREATE TABLE IF NOT EXISTS custom(_id INTEGER PRIMARY KEY ASC, name TEXT, address TEXT, match_name TEXT, match_address TEXT, fav INTEGER);";
		db.execSQL(sql4);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		boolean validUpgrade = false;
		if (oldVersion == 1) {
			if (newVersion >= 2) {
				db.execSQL("DROP TABLE IF EXISTS event");
				validUpgrade = true;
			}
		} else if (oldVersion == 2) {
			if (newVersion >= 3) {
				validUpgrade = true;
			} 
		} else if (oldVersion == 3) {
			if (newVersion == 4) {				
				String sql4 = "ALTER TABLE custom ADD COLUMN fav INTEGER DEFAULT 0";
				db.execSQL(sql4);
				validUpgrade = true;
			} 			
		}

		if (!validUpgrade) {
			db.execSQL("DROP TABLE IF EXISTS event");
			db.execSQL("DROP TABLE IF EXISTS player");
			db.execSQL("DROP TABLE IF EXISTS portal");
			db.execSQL("DROP TABLE IF EXISTS custom");
		}

		onCreate(db);
	}
}
