package com.holgis.portalnotifier.data;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v4.app.Fragment;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.JsonWriter;
import android.widget.Toast;

import com.holgis.portalnotifier.App;

public class Exporter {

	private Activity mActivity;
	private Fragment mFragment;
	private String mVersion;
	private int mBuild;

	public Exporter(Fragment fragment) {

		mFragment = fragment;
		mActivity = fragment.getActivity();

		PackageManager packageManager = mActivity.getPackageManager();
		String packageName = mActivity.getPackageName();

		mVersion = "0.0"; // initialize String
		mBuild = 0;

		try {
			mVersion = packageManager.getPackageInfo(packageName, 0).versionName;
			mBuild = packageManager.getPackageInfo(packageName, 0).versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}

	}

	public void Export(Context context, Uri uri) throws FileNotFoundException {

		ParcelFileDescriptor pfd = mActivity.getContentResolver()
				.openFileDescriptor(uri, "w");

		FileOutputStream fos = new FileOutputStream(pfd.getFileDescriptor());

		BufferedOutputStream bos = new BufferedOutputStream(fos);

		OutputStreamWriter osw = new OutputStreamWriter(bos);

		JsonWriter writer = new JsonWriter(osw);
		writer.setIndent("  ");

		Dataset dataset = new Dataset();

		try {
			int playerExport = 0;
			int portalExport = 0;
			int customExport = 0;

			writer.beginObject();
			writer.name("version").value(mVersion);
			writer.name("build").value(mBuild);

			writer.name("portals").beginArray();
			Cursor c = dataset.QueryPortals();
			if (c != null && c.moveToFirst()) {
				do {
					Portal p = dataset.loadPortal(c, 0);
					writePortal(writer, p);
					++portalExport;
				} while (c.moveToNext());
			}
			c.close();
			writer.endArray();

			writer.name("players").beginArray();
			c = dataset.QueryPlayers();
			if (c != null && c.moveToFirst()) {
				do {
					Player p = dataset.loadPlayer(c, 0);
					writePlayer(writer, p);
					++playerExport;
				} while (c.moveToNext());
			}
			c.close();
			writer.endArray();

			writer.name("custom").beginArray();
			c = dataset.QueryCustoms();
			if (c != null && c.moveToFirst()) {
				do {
					Custom p = dataset.LoadCustom(c);
					writeCustom(writer, p);
					++customExport;
				} while (c.moveToNext());
			}
			c.close();
			writer.endArray();

			writer.endObject();

			writer.flush();
			writer.close();

			pfd.close();

			String message = String
					.format("Export complete!\n%d player, %d portals\nand %d custom portals",
							playerExport, portalExport, customExport);
			Toast.makeText(context, message, Toast.LENGTH_LONG).show();

		} catch (Exception e) {
			String message = String
					.format("Export failed!\n%s", e.getMessage());
			Toast.makeText(context, message, Toast.LENGTH_LONG).show();
		} finally {

		}
	}

	@SuppressLint("DefaultLocale")
	public void Import(Context context, Uri uri) throws FileNotFoundException {

		ParcelFileDescriptor pfd = mActivity.getContentResolver()
				.openFileDescriptor(uri, "r");

		FileInputStream fis = new FileInputStream(pfd.getFileDescriptor());
		BufferedInputStream bis = new BufferedInputStream(fis);
		InputStreamReader isr = new InputStreamReader(bis);

		JsonReader reader = new JsonReader(isr);

		int playerTotal = 0;
		int playerImport = 0;
		int portalsTotal = 0;
		int portalsImport = 0;
		int customTotal = 0;
		int customImport = 0;

		Dataset dataset = new Dataset();

		try {

			reader.beginObject();

			while (JsonToken.NAME == reader.peek()) {

				String tname = reader.nextName();

				if ("portals".equalsIgnoreCase(tname)) {
					reader.beginArray();
					while (JsonToken.BEGIN_OBJECT == reader.peek()) {
						++portalsTotal;
						if (readPortal(dataset, reader)) {
							++portalsImport;
						}
					}
					reader.endArray();

				} else if ("players".equalsIgnoreCase(tname)) {
					reader.beginArray();
					while (JsonToken.BEGIN_OBJECT == reader.peek()) {
						++playerTotal;
						if (readPlayer(dataset, reader)) {
							++playerImport;
						}
					}
					reader.endArray();

				} else if ("custom".equalsIgnoreCase(tname)) {
					reader.beginArray();
					while (JsonToken.BEGIN_OBJECT == reader.peek()) {
						++customTotal;
						if (readCustom(dataset, reader)) {
							++customImport;
						}
					}
					reader.endArray();

				} else {
					reader.skipValue();
				}

			}

			reader.endObject();

			reader.close();
			pfd.close();

			String message = String
					.format("Import complete!\n%d of %d new player, %d of %d new portals\nand %d of %d custom portals",
							playerImport, playerTotal, portalsImport,
							portalsTotal, customImport, customTotal);
			
			Toast.makeText(context, message, Toast.LENGTH_LONG).show();

		} catch (Exception e) {

			String message = String
					.format("Import failed!\n%s", e.getMessage());
			Toast.makeText(context, message, Toast.LENGTH_LONG).show();

		} finally {

		}
	}

	private void writePortal(JsonWriter writer, Portal portal)
			throws IOException {
		writer.beginObject();
		writer.name("name").value(portal.getName());
		writer.name("address").value(portal.getAddress());
		writer.name("favorite").value(portal.isFavourite());
		writer.name("ignore").value(portal.isIgnore());
		if (portal.hasLocation()) {
		}
		writer.endObject();
	}

	private boolean readPortal(Dataset dataset, JsonReader reader)
			throws IOException {

		boolean isNew = false;
		String name = "";
		String adr = "";
		boolean fav = false;
		boolean ignore = false;

		reader.beginObject();

		while (JsonToken.NAME == reader.peek()) {

			String tname = reader.nextName();

			if ("name".equalsIgnoreCase(tname)) {
				name = reader.nextString();
			} else if ("address".equalsIgnoreCase(tname)) {
				adr = reader.nextString();
			} else if ("favorite".equalsIgnoreCase(tname)) {
				fav = reader.nextBoolean();
			} else if ("ignore".equalsIgnoreCase(tname)) {
				ignore = reader.nextBoolean();
			} else {
				reader.skipValue();
			}
		}

		reader.endObject();

		SQLiteDatabase db = App.getDb();

		Portal portal = dataset.loadPortal(db, name, adr);
		if (portal.getId() == -1) {

			portal.setIgnore(ignore);
			portal.setFavourite(fav);
			dataset.savePortal(db, portal);
			isNew = true;
		}
		return isNew;
	}

	private void writePlayer(JsonWriter writer, Player player)
			throws IOException {
		writer.beginObject();
		writer.name("name").value(player.getName());
		writer.name("favorite").value(player.isFavourite());
		writer.name("ignore").value(player.isIgnore());
		writer.endObject();
	}

	private boolean readPlayer(Dataset dataset, JsonReader reader)
			throws IOException {

		boolean isNew = false;

		String name = "";
		boolean fav = false;
		boolean ignore = false;

		reader.beginObject();

		while (JsonToken.NAME == reader.peek()) {

			String tname = reader.nextName();

			if ("name".equalsIgnoreCase(tname)) {
				name = reader.nextString();
			} else if ("favorite".equalsIgnoreCase(tname)) {
				fav = reader.nextBoolean();
			} else if ("ignore".equalsIgnoreCase(tname)) {
				ignore = reader.nextBoolean();
			} else {
				reader.skipValue();
			}
		}
		reader.endObject();

		SQLiteDatabase db = App.getDb();

		Player player = dataset.loadPlayer(db, name);
		if (player.getId() == -1) {

			player.setIgnore(ignore);
			player.setFavourite(fav);

			dataset.savePlayer(db, player);
			isNew = true;
		}
		return isNew;
	}

	private void writeCustom(JsonWriter writer, Custom portal)
			throws IOException {
		writer.beginObject();
		writer.name("name").value(portal.getName());
		writer.name("address").value(portal.getAddress());
		writer.name("favorite").value(portal.isFavourite());
		writer.name("ignore").value(portal.isIgnore());
		writer.endObject();
	}

	private boolean readCustom(Dataset dataset, JsonReader reader)
			throws IOException {

		boolean isNew = false;

		String name = "";
		String address = "";
		boolean fav = true;
		boolean ignore = false;
		
		reader.beginObject();

		while (JsonToken.NAME == reader.peek()) {

			String tname = reader.nextName();

			if ("name".equalsIgnoreCase(tname)) {
				name = reader.nextString();
			} else if ("address".equalsIgnoreCase(tname)) {
				address = reader.nextString();
			} else if ("favorite".equalsIgnoreCase(tname)) {
				fav = reader.nextBoolean();
			} else if ("ignore".equalsIgnoreCase(tname)) {
				ignore = reader.nextBoolean();
			} else {
				reader.skipValue();
			}
		}

		reader.endObject();

		Custom portal = dataset.FindCustom(name, address);
		if (portal == null) {
			portal = new Custom();
			portal.setName(name);
			portal.setAddress(address);
			portal.setIgnore(ignore);
			portal.setFavourite(fav);

			dataset.SaveCustom(portal);

			isNew = true;
		}

		return isNew;
	}

	public void PickWriteFile(String fileName, String contentType,
			int requestCode) {

		Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT); // ACTION_CREATE_DOCUMENT

		// Filter to only show results that can be "opened", such as
		// a file (as opposed to a list of contacts or timezones).
		intent.addCategory(Intent.CATEGORY_OPENABLE);

		// Create a file with the requested MIME type.
		intent.setType(contentType);
		intent.putExtra(Intent.EXTRA_TITLE, fileName);
		mFragment.startActivityForResult(intent, requestCode);
	}

	public void PickReadFile(String fileName, String contentType,
			int requestCode) {

		Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

		// Filter to only show results that can be "opened", such as
		// a file (as opposed to a list of contacts or timezones).
		intent.addCategory(Intent.CATEGORY_OPENABLE);

		// Create a file with the requested MIME type.
		intent.setType("*/*");
		String[] mimetypes = { "application/*", "text/*", "/", contentType };
		intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);

		// intent.setType(contentType);
		intent.putExtra(Intent.EXTRA_TITLE, fileName);
		mFragment.startActivityForResult(intent, requestCode);
	}

}
