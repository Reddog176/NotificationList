package com.holgis.portalnotifier.data;


public class Custom {

	private int _id;
	private String mName;
	private String mAddress;
	private boolean mFavourite;
	private boolean mIgnore;

	public Custom() {
		this._id = -1;
		this.mName = "";
		this.mAddress = "";
		this.mFavourite = true;
		this.mIgnore = false;
	}

	public Custom(int id) {
		this._id = (int) id;
	}

	public int getId(){
		return this._id;
	}

	public String getName() {
		return this.mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public String getAddress() {
		return this.mAddress;
	}

	public void setAddress(String address) {
		this.mAddress = address;
	}
	
	public boolean isFavourite() {
		return this.mFavourite;
	}
	
	public void setFavourite(boolean favourite) {
		this.mFavourite = favourite;
		if(favourite){
			this.mIgnore = false;
		}
	}

	public boolean isIgnore() {
		return this.mIgnore;
	}

	public void setIgnore(boolean ignore) {
		this.mIgnore = ignore;
		if(ignore){
			this.mFavourite = false;
		}

	}

}
