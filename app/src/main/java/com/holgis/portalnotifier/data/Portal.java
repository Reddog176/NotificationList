package com.holgis.portalnotifier.data;

import android.location.Location;

public class Portal {

	private int _id;
	private String mName;
	private String mAddress;
	private boolean mFavourite;
	private boolean mIgnore;

	private Location mLocation;

	public Portal() {
	
		this._id = -1;
		this.mName  = "";
		this.mAddress = "";
		this.mFavourite = false;
		this.mIgnore = false;
		
		this.mLocation = null;
	}

	public Portal(int id) {
		this._id = (int) id;
	}

	public int getId(){
		return this._id;
	}

	public String getName() {
		return this.mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public String getAddress() {
		return this.mAddress;
	}

	public void setAddress(String address) {
		this.mAddress = address;
	}

	public boolean isFavourite() {
		return this.mFavourite;
	}

	public void setFavourite(boolean favourite) {
		this.mFavourite = favourite;
		if(favourite){
			this.mIgnore = false;
		}
	}

	public boolean isIgnore() {
		return this.mIgnore;
	}

	public void setIgnore(boolean ignore) {
		this.mIgnore = ignore;
		if(ignore){
			this.mFavourite = false;
		}
	}

	
	//Location is not used
	public boolean hasLocation() {
		return this.mLocation != null;
	}

	public Location getLocation() {
		return this.mLocation;
	}

	public void setLocation(Location location) {
		this.mLocation = location;
	}

}
