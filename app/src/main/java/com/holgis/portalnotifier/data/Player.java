package com.holgis.portalnotifier.data;

public class Player {

	private int _id;
	private String mName;
	private boolean mFavourite;
	private boolean mIgnore;

	public Player() {
		this._id = -1;
		this.mName = "";
		this.mFavourite = false;
		this.mIgnore = false;
	}

	public Player(int id) {
		this._id = (int) id;
	}
	
	public int getId(){
		return this._id;
	}

	public String getName() {
		return this.mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

	public boolean isFavourite() {
		return this.mFavourite;
	}

	public void setFavourite(boolean favourite) {
		this.mFavourite = favourite;
		if(favourite){
			this.mIgnore = false;
		}
	}

	public boolean isIgnore() {
		return this.mIgnore;
	}

	public void setIgnore(boolean ignore) {
		this.mIgnore = ignore;
		if(ignore){
			this.mFavourite = false;
		}
	}

}
