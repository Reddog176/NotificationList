package com.holgis.portalnotifier.data;


public class Entry {

	private int _id;
	private String mTime;
	private int mType;
	private Player mPlayer;
	private String mMessage;
	private Portal mPortal;
	private long mDate;

	public Entry() {
		this._id = -1;
	}

	public Entry(int id) {
		this._id = (int) id;
	}
	
	public int getId(){
		return this._id;
	}

	public String getTime() {
		return mTime;
	}
	public void setTime(String time) {
		this.mTime = time;
	}
	public int getType() {
		return this.mType;
	}
	public void setType(int type) {
		this.mType = type;
	}
	public Player getPlayer() {
		return this.mPlayer;
	}
	public void setPlayer(Player player) {
		this.mPlayer = player;
	}
	public String getMessage() {
		return this.mMessage;
	}
	public void setMessage(String message) {
		this.mMessage = message;
	}
	public Portal getPortal() {
		return this.mPortal;
	}
	public void setPortal(Portal portal) {
		this.mPortal = portal;
	}
	public long getDate() {
		return this.mDate;
	}
	public void setDate(long date) {
		this.mDate = date;
	}
	
}
