package com.holgis.portalnotifier.ui;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.holgis.portalnotifier.App;
import com.holgis.portalnotifier.R;

public class DisturbActivity extends FragmentActivity {

	private ActionBar mActionBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		App.applySelectedTheme(this);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.disturb_activity);

		mActionBar = getActionBar();
		mActionBar.setHomeButtonEnabled(true);

	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	protected void onPause() {

		super.onPause();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		setIntent(intent);
	}


}
