package com.holgis.portalnotifier.ui;

import android.view.View;
import android.view.ViewParent;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;

public class OnFavClickListener implements OnClickListener, OnLongClickListener {
	
	
	private OnStarItemListener mStartItemListener;

	public OnFavClickListener(OnStarItemListener listener) {
		mStartItemListener = listener;
	}

	@Override
	public boolean onLongClick(View v) {
		click(v, false);
		return true;
	}
	
	@Override
	public void onClick(View v) {
		click(v, true);
	}
	
	private void click(View v, boolean setFav){
		ViewParent vp = v.getParent();
		if (vp != null) {
			View vpv = (View) vp;
			if (vpv != null) {
				mStartItemListener.starItem(vpv.getId(), setFav);
			}
		}			
	}
}
