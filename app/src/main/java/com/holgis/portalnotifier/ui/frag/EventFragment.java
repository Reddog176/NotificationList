package com.holgis.portalnotifier.ui.frag;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.holgis.portalnotifier.R;
import com.holgis.portalnotifier.data.Dataset;
import com.holgis.portalnotifier.data.Entry;
import com.holgis.portalnotifier.service.UpdateEvent;
import com.holgis.portalnotifier.ui.OnFavClickListener;
import com.holgis.portalnotifier.ui.OnStarItemListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class EventFragment extends Fragment implements OnStarItemListener {

	private Dataset dataset = new Dataset();

	private OnFavClickListener mFavListener;

	private View rootView;
	private EventCursorAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.list_fragment, container, false);

		mFavListener = new OnFavClickListener(this);

		Cursor c = query();
		UpdateQuery(c, false);

		adapter = new EventCursorAdapter(getActivity(), c);

		ListView lv = (ListView) rootView.findViewById(android.R.id.list);
		lv.setOnItemClickListener(new ItemClickListener());
		lv.setOnItemLongClickListener(new ItemLongClickListener());
		lv.setAdapter(adapter);

		return rootView;
	}

	@Override
	public void onDestroy() {

		if (adapter != null) {
			adapter.changeCursor(null);
		}

		super.onDestroy();
	}

	private Cursor query() {

		int queryType = 0;

		Cursor c = null;

		Bundle b = getArguments();
		if (b != null) {
			queryType = b.getInt("QUERY");
		}

		switch (queryType) {
		case 3:
			c = dataset.QueryEventsFav();
			break;
		case 2:
			c = dataset.QueryEventsMessages();
			break;
		case 1:
			c = dataset.QueryEventsPortal();
			break;
		case 0:
		default:
			c = dataset.QueryEventsAll();
		}

		return c;
	}

	private void UpdateQuery(Cursor c, boolean changeInAdapter) {
		if (rootView != null) {
			ViewSwitcher vs = (ViewSwitcher) rootView
					.findViewById(R.id.switcher);
			if (vs != null) {
				if (c.getCount() == 0) {
					vs.setDisplayedChild(1);
				} else {
					vs.setDisplayedChild(0);
					if (changeInAdapter) {
						if (adapter != null) {
							adapter.changeCursor(c);
						}
					}
				}
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		EventBus.getDefault().register(this);

		UpdateQuery(query(), true);

	}

	@Override
	public void onPause() {
		super.onPause();

		EventBus.getDefault().unregister(this);

	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onEventMainThread(UpdateEvent event) {
		UpdateQuery(query(), true);
	}

	@Override
	public void starItem(int position, boolean setFav) {

		if (adapter != null) {
			Cursor c = adapter.getCursor();
			c.moveToPosition(position);

			Entry e = dataset.LoadEntry(c);

			if (e.getType() == Dataset.TYPE_CHAT) {
				if (setFav) {
					if (e.getPlayer().isIgnore()) {
						e.getPlayer().setIgnore(false);
					} else {
						e.getPlayer()
								.setFavourite(!e.getPlayer().isFavourite());
					}
				} else {
					if (e.getPlayer().isFavourite()) {
						e.getPlayer().setFavourite(false);
					} else {
						e.getPlayer().setIgnore(!e.getPlayer().isIgnore());
					}
				}
			} else {
				if (setFav) {
					if (e.getPortal().isIgnore()) {
						e.getPortal().setIgnore(false);
					} else {
						e.getPortal()
								.setFavourite(!e.getPortal().isFavourite());
					}
				} else {
					if (e.getPortal().isFavourite()) {
						e.getPortal().setFavourite(false);
					} else {
						e.getPortal().setIgnore(!e.getPortal().isIgnore());
					}
				}
			}

			dataset.UpdateEntry(e);

			// UpdateQuery(query(), true);
			EventBus.getDefault().post(new UpdateEvent());

		}
	}

	class ItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> l, View v, int position, long id) {

			ViewHolder vh = (ViewHolder) v.getTag();

			if (vh != null) {

				if (vh.isExpanded) {
					vh.titleText.setLines(1);
					vh.tickerText.setLines(2);
				} else {
					vh.titleText.setMinLines(1);
					vh.titleText.setMaxLines(Integer.MAX_VALUE);
					vh.tickerText.setMinLines(2);
					vh.tickerText.setMaxLines(Integer.MAX_VALUE);
				}
				vh.isExpanded = !vh.isExpanded;

			}
		}
	}

	class ItemLongClickListener implements ListView.OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> l, View v, int position,
				long id) {
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(getActivity());

			boolean setFav = !prefs.getBoolean("ALLLOW_BLOCKING", false);

			starItem(v.getId(), setFav);
			return true;
		}
	}

	private class ViewHolder {
		ImageView actionIcon;
		TextView titleText;
		TextView tickerText;
		TextView dateText;
		ImageView favIcon;
		boolean isExpanded;
	}

	private class EventCursorAdapter extends CursorAdapter {

		@SuppressWarnings("deprecation")
		public EventCursorAdapter(Context context, Cursor c) {
			super(context, c);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {

			if (view != null) {

				ViewHolder holder = (ViewHolder) view.getTag();
				if (holder == null) {

					holder = new ViewHolder();

					holder.actionIcon = (ImageView) view
							.findViewById(R.id.imageAction);
					holder.titleText = (TextView) view
							.findViewById(R.id.textTitle);
					holder.tickerText = (TextView) view
							.findViewById(R.id.textTicker);
					holder.dateText = (TextView) view
							.findViewById(R.id.textDate);
					holder.favIcon = (ImageView) view
							.findViewById(R.id.imageFav);
					holder.isExpanded = false;

					holder.favIcon.setOnClickListener(mFavListener);

					view.setTag(holder);
				}

				view.setId(cursor.getPosition());

				Entry e = dataset.LoadEntry(cursor);

				boolean fav = false;
				boolean ignore = false;

				switch (e.getType()) {

				case Dataset.TYPE_ATTACK:
					holder.actionIcon.setImageResource(R.drawable.ic_attack);
					holder.titleText.setText(e.getPortal().getName());
					holder.tickerText.setText("under attack by "
							+ e.getPlayer().getName() + "\n("
							+ e.getPortal().getAddress() + ")");
					fav |= e.getPortal().isFavourite();
					ignore |= e.getPortal().isIgnore();

					break;

				case Dataset.TYPE_DESTROY:
					holder.actionIcon.setImageResource(R.drawable.ic_destroy);
					holder.titleText.setText(e.getPortal().getName());
					holder.tickerText.setText("neutralized by "
							+ e.getPlayer().getName() + "\n("
							+ e.getPortal().getAddress() + ")");
					fav |= e.getPortal().isFavourite();
					ignore |= e.getPortal().isIgnore();

					break;

				case Dataset.TYPE_CHAT:
					holder.actionIcon.setImageResource(R.drawable.ic_chat);

					holder.titleText.setText(e.getPlayer().getName());
					holder.tickerText.setText(e.getMessage());

					fav |= e.getPlayer().isFavourite();
					ignore |= e.getPlayer().isIgnore();

					break;
				default:
				}
				holder.dateText.setText(e.getTime());

				holder.favIcon.setImageResource(fav ? R.drawable.ic_favourite
						: ignore ? R.drawable.ic_block
								: R.drawable.ic_not_favourite);
			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {

			LayoutInflater inflater = LayoutInflater.from(context);
			View view = inflater.inflate(R.layout.event_item, parent, false);

			bindView(view, context, cursor);
			return view;
		}
	}

}
