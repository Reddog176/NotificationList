package com.holgis.portalnotifier.ui.frag;

import java.io.FileNotFoundException;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.support.v4.preference.PreferenceFragment;
import android.widget.Toast;

import com.holgis.portalnotifier.App;
import com.holgis.portalnotifier.R;
import com.holgis.portalnotifier.data.Exporter;
import com.holgis.portalnotifier.ui.DisturbActivity;
import com.holgis.portalnotifier.ui.EditActivity;

public class SettingsFragment extends PreferenceFragment implements
		Preference.OnPreferenceClickListener {

	private Exporter mExporter;
	// Unique request code.
	private static final int WRITE_REQUEST_CODE = 43;
	private static final int READ_REQUEST_CODE = 45;
	private static final int SOUND_EVENT_REQUEST_CODE = 47;
	private static final int SOUND_FAV_REQUEST_CODE = 49;
	private static final int SOUND_CUSTOM_REQUEST_CODE = 50;
	private static final int SOUND_MESSAGE_REQUEST_CODE = 51;

	private static final String PREF_REGISTER = "REGISTER";
	private static final String PREF_DARK = "DARK";
	private static final String PREF_EXPORT = "EXPORT";
	private static final String PREF_IMPORT = "IMPORT";
	private static final String PREF_FAV = "FAV_TONE";
	private static final String PREF_CUSTOM = "CUSTOM_TONE";
	private static final String PREF_EVENT = "EVENT_TONE";
	private static final String PREF_MESSAGE = "MESSAGE_TONE";
	private static final String PREF_VERSION = "VERSION";
	private static final String PREF_DISTURB = "DISTURB";
	private static final String CATEGORY_NOTIFICATION = "CATEGORY_NOTIFICATION";
	private static final String PREF_OREO_NOTIFICATIONS = "NOTIFICATION";
	private static final String PREF_SCREEN_FAVORITE = "SCREEN_FAVORITE";
	private static final String PREF_SCREEN_MESSAGE = "SCREEN_MESSAGE";
	private static final String PREF_SCREEN_EVENT= "SCREEN_EVENT";



	private int debugCounter = 0;
	
	private SharedPreferences prefs;

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Load the preferences from an XML resource
		addPreferencesFromResource(R.xml.settings);

		prefs = PreferenceManager
				.getDefaultSharedPreferences(getActivity());

		mExporter = new Exporter(this);


		registerButton(PREF_REGISTER);
		registerButton(PREF_DARK);
		registerButton(PREF_DARK);
		registerButton(PREF_EXPORT);
		registerButton(PREF_IMPORT);
		registerButton(PREF_FAV);
		registerButton(PREF_EVENT);
		registerButton(PREF_CUSTOM);
		registerButton(PREF_MESSAGE);
		registerButton(PREF_VERSION);
		registerButton(PREF_DISTURB);

		PreferenceCategory notificationCategory =
				(PreferenceCategory) findPreference(CATEGORY_NOTIFICATION);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			registerButton(PREF_OREO_NOTIFICATIONS);
			notificationCategory.removePreference(findPreference(PREF_SCREEN_FAVORITE));
			notificationCategory.removePreference(findPreference(PREF_SCREEN_MESSAGE));
			notificationCategory.removePreference(findPreference(PREF_SCREEN_EVENT));
		} else {
			notificationCategory.removePreference(findPreference(PREF_OREO_NOTIFICATIONS));
		}
	}

	@Override
	public void onResume() {
		
		Preference disturb = (Preference) findPreference(PREF_DISTURB);
		
		if(prefs.getBoolean("DISTURB_ENABLE", false)){
			disturb.setSummary(getString(R.string.pref_disturb_summary_on, 
									DisturbFragment.formatTime(getActivity(), prefs.getLong("DISTURB_START", 0)),
									DisturbFragment.formatTime(getActivity(), prefs.getLong("DISTURB_END", 0))
										)
								);
		} else {
			disturb.setSummary(R.string.pref_disturb_summary_off);
		}
		
		super.onResume();
	}
	
	void registerButton(String key) {
		Preference pref = findPreference(key);
		if (pref != null) {
			pref.setOnPreferenceClickListener(this);
		}
	}
	
	@Override
	public boolean onPreferenceClick(Preference pref) {

		if (PREF_REGISTER.equalsIgnoreCase(pref.getKey())) {
			App.registerNotificationService(getActivity());
			return true;
		} else if (PREF_DARK.equalsIgnoreCase(pref.getKey())) {
			getActivity().recreate();
		} else if (PREF_EXPORT.equalsIgnoreCase(pref.getKey())) {
			mExporter.PickWriteFile("notification-helper.json",
					"application/json", WRITE_REQUEST_CODE);
			return true;
		} else if (PREF_IMPORT.equalsIgnoreCase(pref.getKey())) {
			mExporter.PickReadFile("notification-helper.json",
					"application/json", READ_REQUEST_CODE);
			return true;
		} else if (PREF_FAV.equalsIgnoreCase(pref.getKey())) {
			pickSound(SOUND_FAV_REQUEST_CODE);
			return true;
		} else if (PREF_EVENT.equalsIgnoreCase(pref.getKey())) {
			pickSound(SOUND_EVENT_REQUEST_CODE);
			return true;
		} else if (PREF_CUSTOM.equalsIgnoreCase(pref.getKey())) {
			pickSound(SOUND_CUSTOM_REQUEST_CODE);
			return true;
		} else if (PREF_MESSAGE.equalsIgnoreCase(pref.getKey())) {
			pickSound(SOUND_MESSAGE_REQUEST_CODE);
			return true;
		} else if (PREF_DISTURB.equalsIgnoreCase(pref.getKey())) {
			Intent i = new Intent(getActivity(), DisturbActivity.class);
			startActivity(i);
			return true;
		} else if (PREF_OREO_NOTIFICATIONS.equalsIgnoreCase(pref.getKey())) {
			Intent intent = new Intent();
			intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
			//for Android 5-7
			intent.putExtra("app_package", getActivity().getPackageName());
			intent.putExtra("app_uid", getActivity().getApplicationInfo().uid);
			// for Android O
			intent.putExtra("android.provider.extra.APP_PACKAGE", getActivity().getPackageName());
			try {
				startActivity(intent);
			} catch (Exception e) {

			}
			return true;
		} else if (PREF_VERSION.equalsIgnoreCase(pref.getKey())) {
			++debugCounter;
			if(debugCounter>=8){
				boolean debugEnabled = !prefs.getBoolean("DEBUG_NOTIFICATION", false);
				Toast.makeText(getActivity(), debugEnabled ? "Debug Notifications enabled!" : "Debug Notifications disabled!", Toast.LENGTH_LONG).show();
				prefs.edit().putBoolean("DEBUG_NOTIFICATION", debugEnabled).commit();
				debugCounter = 0;
			}
			return true;
		}

		return false;
	}

	private void pickSound(int code) {

		Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);

		intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE,
				RingtoneManager.TYPE_NOTIFICATION);
		intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
		intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI,
				getToneUri(code));

		startActivityForResult(intent, code);
	}

	private void saveSound(int code, Uri uri) {

		String key = getToneKey(code);
		if (!key.isEmpty()) {
			String tone = "";
			if (uri != null) {
				tone = uri.toString();
			}
			prefs.edit().putString(key, tone).commit();
		}
	}

	private String getToneKey(int code) {
		String key;
		switch (code) {
		case SOUND_FAV_REQUEST_CODE:
			key = PREF_FAV;
			break;
		case SOUND_EVENT_REQUEST_CODE:
			key = PREF_EVENT;
			break;
		case SOUND_CUSTOM_REQUEST_CODE:
			key = PREF_CUSTOM;
			break;
		case SOUND_MESSAGE_REQUEST_CODE:
			key = PREF_MESSAGE;
			break;
		default:
			key = "";
		}
		return key;
	}

	private Uri getToneUri(int code) {
		Uri uri = null;
		String tone = prefs.getString(getToneKey(code), "*");
		if ("*".equals(tone)) {
			uri = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		} else if (!tone.isEmpty()) {
			try {
				uri = Uri.parse(tone);
			} catch (Exception e) {
				uri = null;
			}
		}
		return uri;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_OK) {

			if (data != null) {

				switch (requestCode) {

				case WRITE_REQUEST_CODE:
					final Uri writeUri = data.getData();
					new Thread(new Runnable() {
						@Override
						public void run() {
							try {
								mExporter.Export(getActivity(), writeUri);
							} catch (FileNotFoundException e) {
							}
						}
					}).run();
					break;
				case READ_REQUEST_CODE:
					final Uri readUri = data.getData();
					new Thread(new Runnable() {
						@Override
						public void run() {
							try {
								mExporter.Import(getActivity(), readUri);
							} catch (FileNotFoundException e) {
							}
						}
					}).run();
					break;
				case SOUND_FAV_REQUEST_CODE:
				case SOUND_CUSTOM_REQUEST_CODE:
				case SOUND_EVENT_REQUEST_CODE:
				case SOUND_MESSAGE_REQUEST_CODE:
					final Uri toneUri = data
							.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
					saveSound(requestCode, toneUri);
					break;
				}
			}
		}
	}
}