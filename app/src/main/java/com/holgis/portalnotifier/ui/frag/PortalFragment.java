package com.holgis.portalnotifier.ui.frag;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import android.widget.AdapterView.OnItemClickListener;

import com.holgis.portalnotifier.App;
import com.holgis.portalnotifier.R;
import com.holgis.portalnotifier.data.Dataset;
import com.holgis.portalnotifier.data.Portal;
import com.holgis.portalnotifier.service.NotificationHandler;
import com.holgis.portalnotifier.service.UpdateEvent;
import com.holgis.portalnotifier.ui.OnFavClickListener;
import com.holgis.portalnotifier.ui.OnStarItemListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class PortalFragment extends Fragment implements OnStarItemListener {

	private Dataset dataset = new Dataset();

	private OnFavClickListener mFavListener;
	private View rootView;
	private PortalCursorAdapter adapter;

	private SharedPreferences prefs;
	
	private Menu menu;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.list_fragment, container, false);

		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
	
		mFavListener = new OnFavClickListener(this);

		Cursor c = query();
		UpdateQuery(c, false);

		adapter = new PortalCursorAdapter(getActivity(), c);

		ListView lv = (ListView) rootView.findViewById(android.R.id.list);

		lv.setOnItemLongClickListener(new ItemLongClickListener());

		if (prefs.getBoolean("DEBUG_NOTIFICATION", false)) {
			lv.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {

					Cursor c = adapter.getCursor();
					c.moveToPosition(position);

					Portal p = dataset.loadPortal(c, 0);

					NotificationHandler.TestNotification(getActivity(),
							Dataset.TYPE_ATTACK, p.isFavourite());
				}
			});
		}

		lv.setAdapter(adapter);

		setHasOptionsMenu(true);

		return rootView;
	}

	@Override
	public void onDestroy() {

		if (adapter != null) {
			adapter.changeCursor(null);
		}

		super.onDestroy();
	}

	private Cursor query() {
		boolean favOnly = prefs.getBoolean("PORTAL_FRAG_FAV", false);
		Cursor c = favOnly ? dataset.QueryPortalsFav() : dataset.QueryPortals();
		return c;
	}

	private void UpdateQuery(Cursor c, boolean changeInAdapter) {
		if (rootView != null) {
			ViewSwitcher vs = (ViewSwitcher) rootView
					.findViewById(R.id.switcher);
			if (vs != null) {
				if (c.getCount() == 0) {
					vs.setDisplayedChild(1);
				} else {
					vs.setDisplayedChild(0);
					if (changeInAdapter) {
						if (adapter != null) {
							adapter.changeCursor(c);
						}
					}
				}
			}
		}
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fav, menu);
		this.menu = menu;
		
		boolean favOnly = prefs.getBoolean("PORTAL_FRAG_FAV", false);
		updateMenu(favOnly);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_fav:
			boolean favOnly = prefs.getBoolean("PORTAL_FRAG_FAV", false);
			prefs.edit().putBoolean("PORTAL_FRAG_FAV", !favOnly).commit();
			updateMenu(!favOnly);
			UpdateQuery(query(), true);			
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	private void updateMenu(boolean favOnly){
		if(this.menu != null) {
			for (int i = 0; i < this.menu.size(); ++i) {
				if(this.menu.getItem(i).getItemId() == R.id.menu_fav) {
					this.menu.getItem(i).setIcon(getResources().getDrawable(favOnly ? R.drawable.ic_favourite : R.drawable.ic_not_favourite));
					break;
				}		
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		EventBus.getDefault().register(this);
		
		boolean favOnly = prefs.getBoolean("PORTAL_FRAG_FAV", false);
		updateMenu(favOnly);

		UpdateQuery(query(), true);
	}

	@Override
	public void onPause() {
		super.onPause();

		EventBus.getDefault().unregister(this);
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onEventMainThread(UpdateEvent event) {
		UpdateQuery(query(), true);
	}

	@Override
	public void starItem(int position, boolean setFav) {
		Cursor c = adapter.getCursor();
		c.moveToPosition(position);

		Portal p = dataset.loadPortal(c, 0);

		if (setFav) {
			if (p.isIgnore()) {
				p.setIgnore(false);
			} else {
				p.setFavourite(!p.isFavourite());
			}
		} else {
			if (p.isFavourite()) {
				p.setFavourite(false);
			} else {
				p.setIgnore(!p.isIgnore());
			}
		}

		dataset.updatePortal(App.getDb(), p);
		EventBus.getDefault().post(new UpdateEvent());

	}

	class ItemLongClickListener implements ListView.OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> l, View v, int position,
				long id) {

			boolean setFav = !prefs.getBoolean("ALLLOW_BLOCKING", false);
			starItem(v.getId(), setFav);
			return true;
		}
	}

	private class ViewHolder {
		ImageView actionIcon;
		TextView titleText;
		TextView tickerText;
		ImageView favIcon;
	}

	private class PortalCursorAdapter extends CursorAdapter {

		@SuppressWarnings("deprecation")
		public PortalCursorAdapter(Context context, Cursor c) {
			super(context, c);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {

			if (view != null) {

				ViewHolder holder = (ViewHolder) view.getTag();
				if (holder == null) {

					holder = new ViewHolder();

					holder.actionIcon = (ImageView) view
							.findViewById(R.id.imageAction);
					holder.titleText = (TextView) view
							.findViewById(R.id.textTitle);
					holder.tickerText = (TextView) view
							.findViewById(R.id.textTicker);
					holder.favIcon = (ImageView) view
							.findViewById(R.id.imageFav);

					holder.favIcon.setOnClickListener(mFavListener);

					view.setTag(holder);
				}

				view.setId(cursor.getPosition());

				Portal p = dataset.loadPortal(cursor, 0);

				holder.actionIcon.setImageResource(R.drawable.ic_destroy);
				holder.titleText.setText(p.getName());
				holder.tickerText.setText(p.getAddress());

				boolean fav = p.isFavourite();
				boolean ignore = p.isIgnore();

				holder.favIcon.setImageResource(fav ? R.drawable.ic_favourite
						: ignore ? R.drawable.ic_block
								: R.drawable.ic_not_favourite);
			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {

			LayoutInflater inflater = LayoutInflater.from(context);
			View view = inflater.inflate(R.layout.portal_item, parent, false);

			bindView(view, context, cursor);
			return view;
		}
	}

}
