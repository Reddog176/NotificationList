package com.holgis.portalnotifier.ui;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.holgis.portalnotifier.App;
import com.holgis.portalnotifier.R;
import com.holgis.portalnotifier.data.Dataset;
import com.holgis.portalnotifier.service.NotificationService;
import com.holgis.portalnotifier.ui.frag.CursomFragment;
import com.holgis.portalnotifier.ui.frag.EventFragment;
import com.holgis.portalnotifier.ui.frag.PlayerFragment;
import com.holgis.portalnotifier.ui.frag.PortalFragment;
import com.holgis.portalnotifier.ui.frag.SettingsFragment;

public class NotificationActivity extends FragmentActivity {

	private ViewPager mPager;
	private MultiFragmentAdapter mAdapter;
	private ActionBar mActionBar;
	private Dataset mDataset;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		App.applySelectedTheme(this);
		super.onCreate(savedInstanceState);

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		saveFirstBuild(prefs);

		setContentView(R.layout.notification_activity);

		// Initilization
		mPager = (ViewPager) findViewById(R.id.pager);

		mActionBar = getActionBar();

		mAdapter = new MultiFragmentAdapter(getSupportFragmentManager(), this);

		mAdapter.AddFragment(EventFragment.class,
				getString(R.string.fragment_favorites), putArguments(3));
		
		mAdapter.AddFragment(EventFragment.class,
				getString(R.string.fragment_everything), putArguments(0));

		mAdapter.AddFragment(EventFragment.class,
				getString(R.string.fragment_attacks), putArguments(1));

		mAdapter.AddFragment(EventFragment.class,
				getString(R.string.fragment_messages), putArguments(2));
	
		mAdapter.AddFragment(PortalFragment.class,
				getString(R.string.fragment_portals));

		mAdapter.AddFragment(PlayerFragment.class,
				getString(R.string.fragment_players));

		mAdapter.AddFragment(CursomFragment.class,
				getString(R.string.fragment_custom));

		mAdapter.AddFragment(SettingsFragment.class,
				getString(R.string.fragment_settings));

		mPager.setAdapter(mAdapter);

		mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
			}

			@SuppressWarnings("deprecation")
			@Override
			public void onPageSelected(int position) {
				mActionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrollStateChanged(int state) {
			}
		});

		mActionBar.setHomeButtonEnabled(true);

		mAdapter.IntegrateActionbar(mActionBar, mPager);

		mDataset = new Dataset();

	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		// mActionBar.setIcon(BillingHelper.GetBadgeResource(BillingHelper.GetIAPLevel(this)));

		if (!NotificationService.IsRegistered(this)) {

			if (!prefs.getBoolean("REGISTERD", false)) {
				App.registerNotificationService(this);
			} else {
				Toast.makeText(
						this,
						"Notification Listener not registered!!!\nThe App will not work unless you register it!.\nGo to 'Settings' and select 'Activate Notification Helper'",
						Toast.LENGTH_LONG).show();
			}
			prefs.edit().putBoolean("REGISTERD", true).commit();
		}

	}

	@Override
	protected void onPause() {

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		try {
			String removePref = prefs.getString("REMOVE_AFTER", "0");
			int removeAfter = Integer.parseInt(removePref);
			if (removeAfter > 0) {
				mDataset.DeleteEntrys(removeAfter);
			}
		} catch (Exception e) {
		}

		super.onPause();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		setIntent(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_settings:
			mAdapter.setClassItem(SettingsFragment.class);
			break;
		case android.R.id.home:
			mAdapter.setClassItem(EventFragment.class);
			break;
		case R.id.menu_ingress:
			launchIngress(this);
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	void launchIngress(Context context) {

		final PackageManager packageManager = context.getPackageManager();

		try {

			Intent i = getPackageManager().getLaunchIntentForPackage(
					"com.nianticlabs.ingress.prime.qa");
			if(i == null) {
				i = packageManager.getLaunchIntentForPackage("com.nianticproject.ingress");
			}

			if (i != null) {
				startActivity(i);
			} else {
				Toast.makeText(context, R.string.error_ingress,
						Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Bundle putArguments(int queryType) {
		Bundle b = new Bundle();
		b.putInt("QUERY", queryType);
		return b;
	}

	private void saveFirstBuild(SharedPreferences prefs) {

		int code = prefs.getInt("FIRST_BUILD", 0);
		if (code == 0) {
			try {
				PackageInfo pInfo;
				pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

				prefs.edit().putInt("FIRST_BUILD", pInfo.versionCode);

			} catch (NameNotFoundException e) {
			}
		}
	}

}
