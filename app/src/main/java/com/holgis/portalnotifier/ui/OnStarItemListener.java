package com.holgis.portalnotifier.ui;

public interface OnStarItemListener {
	public abstract void starItem(int id, boolean setFav);
	
}
