package com.holgis.portalnotifier.ui.frag;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.holgis.portalnotifier.R;
import com.holgis.portalnotifier.data.Custom;
import com.holgis.portalnotifier.data.Dataset;

public class EditFragment extends Fragment {

	private Dataset dataset = new Dataset();
	private View rootView;
	private EditText name;
	private EditText address;
	private Custom custom;

	private boolean saveCustom;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.edit_fragment, container, false);

		name = (EditText) rootView.findViewById(R.id.editName);
		address = (EditText) rootView.findViewById(R.id.editAddress);

		setHasOptionsMenu(true);

		return rootView;
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();

		LoadCustom();

		saveCustom = true;
	}

	@Override
	public void onPause() {

		SaveCustom();

		super.onPause();
	}

	void LoadCustom() {

		int id = -1;
		Intent i = getActivity().getIntent();
		if (i != null) {
			Bundle extra = i.getExtras();
			if (extra != null) {
				id = extra.getInt("ID", -1);
			}
		}

		custom = dataset.LoadCustom(id);

		if (custom == null) {
			custom = new Custom();
		}

		name.setText(custom.getName());
		address.setText(custom.getAddress());

	}

	void SaveCustom() {

		if (saveCustom && custom != null) {

			String strName = name.getText().toString();

			String strAddress = address.getText().toString();

			if (!strName.isEmpty() || !strAddress.isEmpty()) {
				custom.setName(strName);
				custom.setAddress(strAddress);

				dataset.SaveCustom(custom);
			}
		}
	}

	void DeleteCustom() {
		dataset.DeleteCustom(custom);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.edit, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.edit_action_delete:
			DeleteCustom();
		case R.id.edit_action_cancel:
			saveCustom = false;
			break;
		case R.id.edit_action_done:
			break;
		}

		getActivity().finish();

		return super.onOptionsItemSelected(item);
	}

}
