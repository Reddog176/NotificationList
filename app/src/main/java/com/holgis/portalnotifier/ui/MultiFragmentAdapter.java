package com.holgis.portalnotifier.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

@SuppressWarnings("deprecation")
public class MultiFragmentAdapter extends FragmentPagerAdapter implements
		ActionBar.TabListener {

	public static final String ARG_PAGE_INDEX = "page_index";

	private ViewPager mPager = null;

	private class FragmentInfo {

		public Class<?> Fragment;
		public String Title;
		public Bundle Arguments;

		public FragmentInfo(Class<?> fragment, String title, Bundle arguments) {
			this.Fragment = fragment;
			this.Title = title;
			this.Arguments = arguments;
		}
	}

	private List<FragmentInfo> mFragments = new ArrayList<FragmentInfo>();

	public MultiFragmentAdapter(FragmentManager fm, Context ctx) {
		super(fm);
	}

	public void AddFragment(Class<?> fragment, String title) {
		AddFragment(fragment, title, null);
	}

	public void AddFragment(Class<?> fragment, String title, Bundle arguments) {
		mFragments.add(new FragmentInfo(fragment, title, arguments));
	}

	public static String getFragmentAutoTag(int viewId, int index) {
		return "android:switcher:" + viewId + ":" + index;
	}

	@Override
	public Fragment getItem(int position) {

		Fragment fragment = null;

		FragmentInfo fi = mFragments.get(position);

		if (fi != null) {
			try {
				fragment = (Fragment) fi.Fragment.newInstance();
			} catch (InstantiationException e) {
			} catch (IllegalAccessException e) {
			}
		}

		if (fragment != null) {
			Bundle args = null;
			if (fi.Arguments != null) {
				args = fi.Arguments;
			} else {
				args = new Bundle();
			}
			args.putInt(ARG_PAGE_INDEX, position);
			fragment.setArguments(args);
		}
		return fragment;
	}

	@Override
	public int getCount() {
		return mFragments.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {

		FragmentInfo fi = mFragments.get(position);
		if (fi != null) {
			return fi.Title;
		} else
			return "";
	}

	public void IntegrateActionbar(ActionBar actionBar, ViewPager pager) {
		mPager = pager;
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		for (FragmentInfo info : mFragments) {
			actionBar.addTab(actionBar.newTab().setText(info.Title)
					.setTabListener(this));
		}
	}

	public void setClassItem(Class<?> classname) {
		if (mPager != null) {
			int pos = 0;
			for (FragmentInfo info : mFragments) {
				if (info.Fragment == classname) {
					mPager.setCurrentItem(pos);
					return;
				}
				++pos;
			}
		}
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		if (mPager != null) {
			mPager.setCurrentItem(tab.getPosition());
		}
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}
}