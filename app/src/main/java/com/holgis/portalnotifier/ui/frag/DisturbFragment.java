package com.holgis.portalnotifier.ui.frag;

import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TimePicker;

import com.holgis.portalnotifier.R;

public class DisturbFragment extends Fragment implements OnClickListener {

	private View rootView;

	private CheckBox enable;
	private Button start;
	private Button stop;
	
	private long startTime = 0;
	private long stopTime = 0;
	
	private boolean cancel = false;
	private int theme = TimePickerDialog.THEME_DEVICE_DEFAULT_DARK;
	boolean is24 = true;

	private SharedPreferences prefs = null;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.disturb_fragment, container, false);

		enable = (CheckBox) rootView.findViewById(R.id.enableDisturb);
		start = (Button) rootView.findViewById(R.id.startDisturb);
		stop = (Button) rootView.findViewById(R.id.endDisturb);

		start.setOnClickListener(this);
		stop.setOnClickListener(this);
		
		setHasOptionsMenu(true);
		
		prefs = PreferenceManager
				.getDefaultSharedPreferences(getActivity());

		 theme = prefs.getBoolean("DARK", true) ? TimePickerDialog.THEME_DEVICE_DEFAULT_DARK : 
			 TimePickerDialog.THEME_DEVICE_DEFAULT_LIGHT;
	       
		 is24 = android.text.format.DateFormat.is24HourFormat(getActivity());

		return rootView;
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();

		enable.setChecked(prefs.getBoolean("DISTURB_ENABLE", false));
		
		startTime = prefs.getLong("DISTURB_START", 0);
		stopTime = prefs.getLong("DISTURB_END", 0);
		
		start.setText(formatTime(getActivity(), startTime));
		stop.setText(formatTime(getActivity(), stopTime));
			
		cancel = false;
	}

	@Override
	public void onPause() {

		if(!cancel) {
			prefs.edit()
				.putBoolean("DISTURB_ENABLE", enable.isChecked())
				.putLong("DISTURB_START", startTime)
				.putLong("DISTURB_END", stopTime)
			.commit();
		}
		
		super.onPause();
	}


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.disturb, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.edit_action_cancel:
			cancel = true;
			break;
		case R.id.edit_action_done:
			cancel = false;
			break;
		}

		getActivity().finish();

		return super.onOptionsItemSelected(item);
	}
	
	@SuppressLint("DefaultLocale")
	static public String formatTime(Context context, long time){

		Calendar cal = Calendar.getInstance();
			
		cal.set(Calendar.HOUR_OF_DAY, (int) (time/100));
		cal.set(Calendar.MINUTE, (int) (time%100));
		cal.set(Calendar.SECOND, 0);
		
		java.text.DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(context);
		return timeFormat.format(cal.getTime());
		
	}

	@Override
	public void onClick(View v) {
		
		switch(v.getId())
		{
		case R.id.startDisturb:
            new TimePickerDialog(getActivity(), theme, new TimePickerDialog.OnTimeSetListener() {			
				@Override
				public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					startTime = hourOfDay * 100 + minute;
					start.setText(formatTime(getActivity(), startTime));
				}
			}, (int)(startTime/ 100), (int)(startTime% 100), is24).show();
			break;
		case R.id.endDisturb:
            new TimePickerDialog(getActivity(), theme, new TimePickerDialog.OnTimeSetListener() {			
				@Override
				public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					stopTime = hourOfDay * 100 + minute;
					stop.setText(formatTime(getActivity(), stopTime));
				}
			}, (int)(stopTime/ 100), (int)(stopTime% 100), is24).show();
         break;
		}
	}

}
