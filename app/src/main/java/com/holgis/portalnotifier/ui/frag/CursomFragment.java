package com.holgis.portalnotifier.ui.frag;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.holgis.portalnotifier.R;
import com.holgis.portalnotifier.data.Custom;
import com.holgis.portalnotifier.data.Dataset;
import com.holgis.portalnotifier.service.UpdateEvent;
import com.holgis.portalnotifier.ui.EditActivity;
import com.holgis.portalnotifier.ui.OnFavClickListener;
import com.holgis.portalnotifier.ui.OnStarItemListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class CursomFragment extends Fragment implements OnStarItemListener {

	private OnFavClickListener mFavListener;

	private Dataset dataset = new Dataset();
	private View rootView;
	private CustomCursorAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.list_fragment, container, false);

		mFavListener = new OnFavClickListener(this);

		Cursor c = query();
		UpdateQuery(c, false);

		adapter = new CustomCursorAdapter(getActivity(), c);

		ListView lv = (ListView) rootView.findViewById(android.R.id.list);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new CustomClickListener());
		lv.setOnItemLongClickListener(new CustomLongClickListener());

		setHasOptionsMenu(true);

		return rootView;
	}

	@Override
	public void onDestroy() {

		if (adapter != null) {
			adapter.changeCursor(null);
		}

		super.onDestroy();
	}

	private Cursor query() {
		Cursor c = dataset.QueryCustoms();
		return c;
	}

	private void UpdateQuery(Cursor c, boolean changeInAdapter) {
		if (rootView != null) {
			ViewSwitcher vs = (ViewSwitcher) rootView
					.findViewById(R.id.switcher);
			if (vs != null) {
				if (c.getCount() == 0) {
					vs.setDisplayedChild(1);
				} else {
					vs.setDisplayedChild(0);
					if (changeInAdapter) {
						if (adapter != null) {
							adapter.changeCursor(c);
						}
					}
				}
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		EventBus.getDefault().register(this);

		UpdateQuery(query(), true);
	}

	@Override
	public void onPause() {
		super.onPause();

		EventBus.getDefault().unregister(this);
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onEventMainThread(UpdateEvent event) {
		UpdateQuery(query(), true);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.custom, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_custom_new:
			editCustom(-1);
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void starItem(int position, boolean setFav) {

		Cursor c = adapter.getCursor();
		c.moveToPosition(position);

		Custom p = dataset.LoadCustom(c);

	
		p.setIgnore(!setFav);
		p.setFavourite(setFav);
		
		dataset.SaveCustom(p);

		EventBus.getDefault().post(new UpdateEvent());

	}

	private class CustomClickListener implements ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> list, View view, int position,
				long id) {

			Cursor c = adapter.getCursor();
			c.moveToPosition(view.getId());

			Custom p = dataset.LoadCustom(c);

			editCustom(p.getId());
		}
	}

	private class CustomLongClickListener implements
			ListView.OnItemLongClickListener {

		@Override
		public boolean onItemLongClick(AdapterView<?> l, View v, int position,
				long id) {
			
			SharedPreferences prefs = PreferenceManager
					.getDefaultSharedPreferences(getActivity());

			boolean setFav = !prefs.getBoolean("ALLLOW_BLOCKING", false);

			starItem(v.getId(), setFav);
			return true;
		}
	}

	private void editCustom(int id) {

		try {

			Intent i = new Intent(getActivity(), EditActivity.class);
			i.putExtra("ID", id);

			startActivity(i);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class ViewHolder {
		TextView titleText;
		TextView tickerText;
		ImageView favIcon;
	}

	private class CustomCursorAdapter extends CursorAdapter {

		@SuppressWarnings("deprecation")
		public CustomCursorAdapter(Context context, Cursor c) {
			super(context, c);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {

			if (view != null) {

				ViewHolder holder = (ViewHolder) view.getTag();
				if (holder == null) {

					holder = new ViewHolder();

					holder.titleText = (TextView) view
							.findViewById(R.id.textTitle);
					holder.tickerText = (TextView) view
							.findViewById(R.id.textTicker);
					holder.favIcon = (ImageView) view
							.findViewById(R.id.imageFav);

					holder.favIcon.setOnClickListener(mFavListener);
					holder.favIcon.setOnLongClickListener(mFavListener);

					view.setTag(holder);
				}

				Custom c = dataset.LoadCustom(cursor);

				view.setId(cursor.getPosition());

				holder.titleText.setText(c.getName());
				holder.tickerText.setText(c.getAddress());

				holder.favIcon
						.setImageResource(c.isIgnore() ? R.drawable.ic_block
								: R.drawable.ic_favourite);
			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {

			LayoutInflater inflater = LayoutInflater.from(context);
			View view = inflater.inflate(R.layout.custom_item, parent, false);

			bindView(view, context, cursor);
			return view;
		}
	}

}
