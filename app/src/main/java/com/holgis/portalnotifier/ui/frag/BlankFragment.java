package com.holgis.portalnotifier.ui.frag;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.holgis.portalnotifier.R;

public class BlankFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// The last two arguments ensure LayoutParams are inflated
		// properly.
		View rootView = inflater.inflate(R.layout.blank_fragment, container,
				false);

		TextView tv = (TextView) rootView.findViewById(R.id.text);
		tv.setText(this.getClass().toString());
		return rootView;
	}
}
